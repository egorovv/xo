

#include <dbglog/dbglog.h>
#include <xo/ring.h>
#include <xo/glob.h>
#include <xo/perf.h>

#include <stdlib.h>
#include <argp.h>

static struct argp_option options[] = {
    {"log",    'l', "file",      0, "counters" },
    {"enable", 'e', "pattern",   0, "disable logs matching parren" },
    {"disable",'d', "pattern",   0, "enable logs matching parren" },
    {"print",  'p', "pattern",   0, "print entries matching parren" },
    {"clear",  'c', 0, OPTION_ARG_OPTIONAL, "clear log" },
    { 0 }
};

struct arguments
{
    const char *file;
    const char *enable;
    const char *disable;
    const char *print;
    int         clear;
};

static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
    struct arguments *args = state->input;

    switch (key)
    {
    case 'l':
        args->file = arg;
        break;
    case 'e':
        args->enable = arg;
        break;
    case 'd':
        args->disable = arg;
        break;
    case 'p':
        args->print = arg;
        break;
    case 'c':
        args->clear = 1;
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}


int main(int argc, char** argv)
{
    struct argp argp = { options, parse_opt};
    struct arguments args = {
        .file = "/tmp/perf",
    };
    argp_parse (&argp, argc, argv, 0, 0, &args);

    struct xo_perf_file f;

    xo_perf_open(args.file, &f);

    struct xo_perf_counter *e = f.start;
    for (;e != f.end; ++e) {
        if (args.enable && xo_glob_match(args.enable, e->name)) {
            e->on = 1;
        }

        if (args.disable && xo_glob_match(args.disable, e->name)) {
            e->on = 0;
        }

        if (args.print && xo_glob_match(args.print, e->name)) {
            uint64_t sum, num, avg;
            sum = e->sum;
            num = e->num;
            avg = num ? sum/num : 0;
            printf("%s \t%ld \t%ld \t%ld\n", e->name, num, sum, avg);
        }

        if (args.clear) {
            e->num = 0;
            e->sum = 0;
        }
    }

    if (args.enable || args.disable || args.print) {
        return 0;
    }

    return 0;

}
