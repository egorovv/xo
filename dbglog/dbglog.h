#pragma once

#include <xo/log.h>

#include <sys/time.h>

struct xo_debug_time {
    uint64_t       sec;
    uint64_t       usec;
    uint64_t       tsc;
    uint64_t       res;
};

struct xo_debug_map_head {
    struct xo_debug_time ts;
    struct xo_log_spec   map[0];
};

struct xo_file_map {
    int fd;
    size_t sz;
    void * addr;
};

struct xo_debug_map {
    struct xo_file_map file;
    struct xo_debug_map_head *head;
    struct xo_log_spec *start;
    struct xo_log_spec *end;
};

struct xo_debug_file {
    struct xo_file_map file;
    struct xo_ring *ring;
};

struct xo_debug_ctx {
    struct xo_debug_time ts;
    struct xo_debug_map  map;
    struct xo_debug_file log;
    struct xo_debug_file all;
};

int xo_debug_init(const char *dir);
int xo_debug_create(struct xo_debug_ctx *ctx, const char *dir);
void xo_debug_close(struct xo_debug_ctx *ctx);

int xo_debug_map_open(const char *path, struct xo_debug_map* map);
void xo_debug_map_close(struct xo_debug_map* map);
int xo_debug_file_open(const char *path, struct xo_debug_file *f);
void xo_debug_file_close(struct xo_debug_file* f);

int xo_debug_map_create(const char *path, struct xo_debug_map* map);
int xo_debug_file_create(const char *path, struct xo_debug_file *f);

size_t xo_debug_format(char *buf, size_t sz, uint8_t *msg, size_t msz,
                       struct xo_debug_map *map);

void xo_debug_log(struct xo_log_spec *e, const char *fmt, ...)
        __attribute__((format (printf,2,3)));

#define XO_DEBUG_LOG(l, f, ...) XO_LOGF(xo_debug_log, l, f, ##__VA_ARGS__)


struct xo_perf_file {
    struct xo_file_map file;
    struct xo_perf_counter *start;
    struct xo_perf_counter *end;
};

int xo_perf_create(const char *path, struct xo_perf_file *f);
int xo_perf_open(const char *path, struct xo_perf_file *f);
