
/*
 * debug log:
 *   header : num  - num descs
 *            ring - ring off 
 *            descs[num]
 *            ring
 *   debug map
 */

#include "dbglog.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <pthread.h>
#include <sys/types.h>

#include <xo/ring.h>
#include <xo/perf.h>
#include <xo/buf.h>
#include <xo/rdtsc.h>
#include <xo/strl.h>


// missing gettid
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)

static struct xo_debug_ctx ctx;

const uint64_t xo_debug_res = 100000; // .1s

const size_t xo_debug_chunk_sz = 4*4096;
const size_t xo_debug_chunk_nr = 1024;

static __thread uint32_t xo_log_seq = 0;

struct xo_debug_head {
    int      id;
    uint64_t tsc;
    uint64_t seq;
    uint64_t tid;
};

static void
xo_debug_timestamp(struct xo_debug_time *ts)
{
    struct timeval tv;
    uint64_t tsc;

    tsc = xo_rdtsc();
    usleep(xo_debug_res);
    ts->tsc = xo_rdtsc();

    gettimeofday(&tv, 0);
    ts->sec  = tv.tv_sec;
    ts->usec = tv.tv_usec;

    ts->res = ts->tsc - tsc;
}



static int
xo_file_map_create(const char *path, size_t sz, struct xo_file_map* map)
{
    int fd = open(path, O_RDWR|O_CREAT|O_CLOEXEC, 0666);

    if (fd  == -1) {
        return -1;
    }

    if (ftruncate(fd, sz)) {
        close(fd);
        return -1;
    }

    map->addr = mmap(0, sz, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (map->addr == MAP_FAILED) {
        close(fd);
        return -1;
    }

    map->fd = fd;
    map->sz = sz;
    
    return 0;
}

static int
xo_file_map_open(const char *path, struct xo_file_map* map)
{
    int fd = open(path, O_RDWR);

    if (fd  == -1) {
        return -1;
    }

    size_t sz = lseek(fd, 0, SEEK_END);

    map->addr = mmap(0, sz, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (map->addr == MAP_FAILED) {
        close(fd);
        return -1;
    }

    map->fd = fd;
    map->sz = sz;
    return 0;
}

static void
xo_file_map_close(struct xo_file_map* m)
{
    if (m->addr != MAP_FAILED) {
        munmap(m->addr, m->sz);
    }

    if (m->fd != -1) {
        close(m->fd);
    }
}

int
xo_debug_map_create(const char *path, struct xo_debug_map* map)
{
    size_t sz = xo_log_count() * sizeof(struct xo_log_spec) +
        sizeof (struct xo_debug_map_head);

    if (xo_file_map_create(path, sz, &map->file)) {
        return -1;
    }

    map->head = map->file.addr;
    map->start = map->head->map;
    map->end = map->start + xo_log_count();

    xo_log_init(map->start);
    xo_debug_timestamp(&map->head->ts);

    return 0;
}

int
xo_debug_map_open(const char *path, struct xo_debug_map* map)
{
    if (xo_file_map_open(path, &map->file)) {
        return -1;
    }

    size_t sz = map->file.sz;
    
    if (sz <= sizeof (struct xo_debug_map_head) ||
        (sz - sizeof (struct xo_debug_map_head)) % sizeof(*map->start)) {
        xo_file_map_close(&map->file);
        return -1;
    }

    map->head = map->file.addr;
    map->start = map->head->map;
    map->end = map->start + (sz - sizeof (struct xo_debug_map_head)) / sizeof(*map->start);
    return 0;
}

void
xo_debug_map_close(struct xo_debug_map* map)
{
    xo_file_map_close(&map->file);
}

int
xo_debug_file_create(const char *path, struct xo_debug_file *f)
{
    size_t sz =  sizeof (struct xo_ring) + xo_debug_chunk_sz * xo_debug_chunk_nr;

    if (xo_file_map_create(path, sz, &f->file)) {
        return -1;
    }

    f->ring = f->file.addr;

    xo_ring_init(f->ring, xo_debug_chunk_sz, xo_debug_chunk_nr);

    return 0;
}

int
xo_debug_file_open(const char *path, struct xo_debug_file *f)
{
    if (xo_file_map_open(path, &f->file)) {
        return -1;
    }

    if (f->file.sz <= sizeof (struct xo_ring)) {
        xo_file_map_close(&f->file);
        return -1;
    }

    f->ring = f->file.addr;

    if (f->file.sz != sizeof (struct xo_ring) + f->ring->size) {
        xo_file_map_close(&f->file);
        return -1;
    }

    return 0;
}

void
xo_debug_file_close(struct xo_debug_file* f)
{
    xo_file_map_close(&f->file);
}

void
xo_debug_close(struct xo_debug_ctx *ctx)
{
    xo_debug_file_close(&ctx->all);
    xo_debug_file_close(&ctx->log);
    xo_debug_map_close(&ctx->map);
}

int
xo_debug_create(struct xo_debug_ctx *ctx, const char *dir)
{
    const size_t pathsz = 256;
    char path[pathsz];


    xo_strlcpy(path, dir, pathsz);
    xo_strlcat(path, "/debugmap", pathsz);

    if (xo_debug_map_create(path, &ctx->map)) {
        return -1;
    }

    xo_strlcpy(path, dir, pathsz);
    xo_strlcat(path, "/debuglog", pathsz);
    if (xo_debug_file_create(path, &ctx->log)) {
        xo_debug_map_close(&ctx->map);
        return -1;
    }

    xo_strlcpy(path, dir, pathsz);
    xo_strlcat(path, "/debugall", pathsz);
    if (xo_debug_file_create(path, &ctx->all)) {
        xo_debug_file_close(&ctx->log);
        xo_debug_map_close(&ctx->map);
        return -1;
    }
    return 0;
}

int
xo_debug_init(const char * dir)
{
    if (xo_debug_create(&ctx, dir)) {
        return -1;
    }
    ctx.ts = ctx.map.head->ts;
    return 0;
}

static size_t
xo_debug_pack_head(struct xo_buf *b, struct xo_debug_head *h)
{
    int tag = 0;
    size_t sz = 0;

    sz += xo_buf_move(b, xo_msg_pack_int   (b->buf, b->sz, tag++, h->id));
    sz += xo_buf_move(b, xo_msg_pack_ufixed(b->buf, b->sz, tag++, h->tsc));
    sz += xo_buf_move(b, xo_msg_pack_uint  (b->buf, b->sz, tag++, h->tid));
    sz += xo_buf_move(b, xo_msg_pack_uint  (b->buf, b->sz, tag++, h->seq));
    return sz;
}

int xo_buf_get_int(struct xo_buf *b, uint32_t tag, struct xo_msg_field *f)
{
    size_t sz = xo_msg_parse(b->buf, b->sz, f);
    if (sz > b->sz || f->tag != tag || f->type == xo_msg_type_len) {
        return -1 ;
    }
    xo_buf_move(b, sz);
    return 0;
}

static int
xo_debug_unpack_head(struct xo_buf *b, struct xo_debug_head *h)
{
    uint32_t tag = 0;
    struct xo_msg_field f;
    if (xo_buf_get_int(b, tag++, &f)) {
        return -1;
    }
    h->id = xo_msg_get_int(&f);
    if (xo_buf_get_int(b, tag++, &f)) {
        return -1;
    }
    h->tsc = xo_msg_get_ufixed(&f);
    if (xo_buf_get_int(b, tag++, &f)) {
        return -1;
    }
    h->tid = xo_msg_get_uint(&f);
    if (xo_buf_get_int(b, tag++, &f)) {
        return -1;
    }
    h->seq = xo_msg_get_uint(&f);
    return 0;
}

XO_PERF_COUNTER(xo_debug_log);
XO_PERF_COUNTER(xo_debug_pack);
XO_PERF_COUNTER(xo_debug_head);
XO_PERF_COUNTER(xo_debug_ring);

static inline void
xo_debug_vlog(struct xo_log_spec *e, const char *fmt, va_list ap)
{
    size_t sz;
    uint8_t buf[4096];
    struct xo_buf b = { sizeof(buf), buf};
    XO_PERF_TIMER(tmr);
    XO_PERF_START(tmr, xo_debug_head);

    struct xo_debug_head h = {
        .id = e->id,
        .tsc = xo_rdtsc(),
        .tid = gettid(),
        .seq = xo_log_seq++,
    };

    if (!ctx.all.ring) {
        return;
    }

    xo_atomic_add32(&e->count, 1);

    XO_PERF_START(tmr, xo_debug_pack);
    sz = xo_debug_pack_head(&b, &h);

    sz += xo_buf_move(&b, xo_log_va_pack(b.buf, b.sz, e, ap));

    XO_PERF_START(tmr, xo_debug_ring);
    if(sz < sizeof(buf)) {
        xo_ring_write(ctx.all.ring, buf, sz);
        if (e->level < XO_LOG_INFO && ctx.log.ring != 0) {
            xo_ring_write(ctx.log.ring, buf, sz);
        }
    }
    XO_PERF_STOP(tmr);
}

void
xo_debug_log(struct xo_log_spec *e, const char *fmt, ...)
{
    va_list ap;

    XO_PERF_TIMER(tmr);
    XO_PERF_START(tmr, xo_debug_log);

    va_start(ap, fmt);
    xo_debug_vlog(e, fmt, ap);
    va_end(ap);

    XO_PERF_STOP(tmr);
}



size_t
xo_debug_format(char *buf, size_t len, uint8_t *msg, size_t msz, struct xo_debug_map *map)
{
    struct xo_log_spec *e = 0;
    struct xo_debug_head h;

    struct xo_buf b = {msz, msg};
    struct xo_buf out = { len, (uint8_t*)buf};
    size_t sz = 0;

    if (xo_debug_unpack_head(&b, &h) ) {
        return -1;
    }

    if (h.id != -1) {
        e = xo_log_get_spec(h.id, map->start, map->end);
        if (!e) {
            return -1;
        }
    }

    uint64_t res = map->head->ts.res;
    uint64_t delta = (h.tsc - map->head->ts.tsc);

    uint64_t usec = delta * xo_debug_res / res;
    uint64_t secs = usec / 1000000;
    // under usec cycles
    uint64_t cycles = delta % (res/xo_debug_res) ;

    // under sec usecs
    usec = usec % 1000000;

    // add staring secs
    secs += map->head->ts.sec;

    // roll over starting usecs
    secs += (map->head->ts.usec + usec) / 1000000;
    usec =  (map->head->ts.usec + usec) % 1000000;

    struct tm tm;
    gmtime_r((time_t*)&secs, &tm);

    const char *n = e ? xo_log_level_name(e->level) : " ";
    const char *f = e ? e->func : " ";
    int l = e ? e->line : 0;

    sz += xo_buf_printf(&out, "%02d/%02d/%02d %02d:%02d:%02d.%06lu:%04lu",
                        tm.tm_year%100,tm.tm_mon,tm.tm_mday,
                        tm.tm_hour,tm.tm_min,tm.tm_sec, usec, cycles);

    sz += xo_buf_printf(&out, "|%.1s|%lu/%lu|%s:%d|", n, h.tid, h.seq, f, l);

    if (e && e->nargs >= 0) {
        sz += xo_buf_fmt(&out, e->fmt, e->args, e->nargs, b.buf, b.sz);
    } else if (b.sz > 0) {
        sz += xo_buf_printf(&out, "%.*s",  (int)b.sz, b.buf);
    }

    return sz;
}

int xo_perf_create(const char *path, struct xo_perf_file *f)
{
    size_t sz = xo_perf_count() * sizeof (struct xo_perf_counter);

    if (xo_file_map_create(path, sz, &f->file)) {
        return -1;
    }

    f->start = f->file.addr;
    f->end = f->start + xo_perf_count();
    xo_perf_init(f->start);

    return 0;

}

int xo_perf_open(const char *path, struct xo_perf_file *f)
{
    if (xo_file_map_open(path, &f->file)) {
        return -1;
    }

    if (f->file.sz % sizeof (struct xo_perf_counter)) {
        return -1;
    }

    f->start = f->file.addr;
    f->end = f->start + f->file.sz / sizeof (struct xo_perf_counter);

    return 0;
}
