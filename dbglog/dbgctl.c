

#include <dbglog/dbglog.h>
#include <xo/ring.h>
#include <xo/glob.h>

#include <stdlib.h>
#include <argp.h>

static struct argp_option options[] = {
    {"log",    'l', "file",      0, "debug log" },
    {"map",    'm', "file",      0, "debug map" },
    {"enable", 'e', "pattern",   0, "disable logs matching parren" },
    {"disable",'d', "pattern",   0, "enable logs matching parren" },
    {"print",  'p', "pattern",   0, "print entries matching parren" },
    {"follow", 'f', 0, OPTION_ARG_OPTIONAL, "follow log" },
    {"clear",  'c', 0, OPTION_ARG_OPTIONAL, "clear log" },
    { 0 }
};

struct arguments
{
    const char *file;
    const char *map;
    const char *enable;
    const char *disable;
    const char *print;
    int         follow;
    int         clear;
};

static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
    struct arguments *args = state->input;

    switch (key)
    {
    case 'l':
        args->file = arg;
        break;
    case 'm':
        args->map = arg;
        break;
    case 'e':
        args->enable = arg;
        break;
    case 'd':
        args->disable = arg;
        break;
    case 'p':
        args->print = arg;
        break;
    case 'f':
        args->follow = 1;
        break;
    case 'c':
        args->clear = 1;
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}


int main(int argc, char** argv)
{
    struct argp argp = { options, parse_opt};
    struct arguments args = {
        .file = "/tmp/debugall",
        .map = "/tmp/debugmap",
        .enable = 0,
        .disable = 0,
        .follow = 0,
        .clear = 0,
    };
    argp_parse (&argp, argc, argv, 0, 0, &args);

    struct xo_debug_map map;
    struct xo_debug_file log;

    xo_debug_map_open(args.map, &map);
    xo_debug_file_open(args.file, &log);

    struct xo_log_spec *e = map.start;
    for (;e != map.end; ++e) {
        if (args.enable && xo_glob_match(args.enable, e->func)) {
            e->on = 1;
        }
        if (args.disable && xo_glob_match(args.disable, e->func)) {
            e->on = 0;
        }

        if (args.print && xo_glob_match(args.print, e->func)) {
            printf ("%d \t%s \t%s:%d \t%s \t%s\n",
                    e->count, xo_log_level_name(e->level),
                    e->func, e->line, e->on ? "on" : "off" , e->file);
        }
    }

    if (args.enable || args.disable || args.print) {
        return 0;
    }

    struct xo_ring_pos pos = {};

    size_t totsz = 0;
    size_t totcnt = 0;


    if (args.clear) {
        xo_ring_clear(log.ring);
    }

    uint32_t len;
    uint8_t *data;

    xo_ring_tail_start(log.ring, &pos);

    if (args.follow) {
        int avail = xo_ring_avail(log.ring);
        do {
            avail -= (pos.head - pos.pos);
            xo_ring_tail_start(log.ring, &pos);
        } while (avail);
    }

    while (1) {
        while ((data = xo_ring_tail_next(log.ring, &pos, &len))) {
            totsz += len;
            totcnt += 1;

            char out[4096];
            size_t sz = xo_debug_format(out, sizeof(out), data, len, &map);
            if (sz != (size_t)-1) {
                sz = xo_min(sz, sizeof(out));
                if (out[sz - 1] == '\n') {
                    out[sz] = 0;
                    --sz;
                }
                printf("%.*s\n", (int)sz - 1, out);
            }
        }
        
        if (args.follow) {
            xo_yield();
        } else {
            break;
        }
    }

    if (totcnt) {
        printf ("total %lu bytes / %lu recs = %lu \n", totsz, totcnt, totsz/totcnt);
    }

    return 0;

}
