
(require 'subr-x)

(defun remove-env (var) 
  (cl-remove-if '(lambda (x) (string-match (format "^%s=*" var) x))
                process-environment))

(defun replace-env (var val) 
  (setq process-environment
        (append (list (concat var "=" val))
                (remove-env var))))

(defun read-lines (filePath)
  "Return a list of lines of a file at filePath."
  (with-temp-buffer
    (insert-file-contents filePath)
    (split-string (buffer-string) "\n" t)))

(defun xo-hook()
  (let ((xopath-dir (locate-dominating-file buffer-file-name ".xo"))
        (xopath-file nil)
        (path nil))
    (make-local-variable 'process-environment)
    (make-local-variable 'exec-path)
    (setq xopath-dir (expand-file-name (if xopath-dir xopath-dir "~/")))
    (setq xopath-file (concat xopath-dir ".xo"))
    (replace-env "XOPATH"  (substring xopath-dir 0 -1))
    (dolist (line (read-lines xopath-file) "#")
      (let* ((line (car (split-string line "#")))
             (pos (string-match "=" line)))
        (if pos
            (let* ((var (string-remove-prefix "export " (substring line 0 pos)))
                   (val (substitute-env-vars (substring line (+ pos 1)))))
              (replace-env var val)
              (if (string-equal var "PATH")
                  (setq exec-path (append (split-string val ":" t) exec-path))
                )))))
        ))

(add-hook `find-file-hook (lambda ()
            (when (member (file-name-extension buffer-file-name) xo-mode-alist)
              (xo-hook))))


(defvar xo-mode-alist ()
  "List of filename extensions to enable xo hook")


(setq xo-mode-alist `("go"))
