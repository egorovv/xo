#pragma once


#include "xo.h"


static inline uint64_t xo_rdtsc()
{
   uint32_t lo, hi;
   __asm__ volatile("rdtsc": "=a" (lo), "=d" (hi));
   return (uint64_t)lo | (uint64_t)hi << 32;
}

static inline uint64_t xo_rdtscp()
{
   uint32_t lo, hi;
   __asm__ volatile("rdtscp": "=a" (lo), "=d" (hi)::"%rcx");
   return (uint64_t)lo | (uint64_t)hi << 32;
}


static inline uint64_t xo_rdtsc_start() {
   uint32_t lo, hi;
   __asm__ volatile ("cpuid\n\t"
                     "rdtsc\n\t" : "=a" (lo), "=d" (hi)::
                     "%rbx", "%rcx");
   return (uint64_t)lo | (uint64_t)hi << 32;
}

static inline uint64_t xo_rdtsc_stop() {
   uint32_t lo, hi;
   __asm__ volatile("rdtscp\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    "cpuid\n\t": "=r" (hi), "=r" (lo)::
                    "%rax", "%rbx", "%rcx", "%rdx");
   return (uint64_t)lo | (uint64_t)hi << 32;
}
