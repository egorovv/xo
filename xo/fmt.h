#pragma once

#include "xo.h"
#include "msg.h"
#include <stdarg.h>

enum xo_fmt_arg_type {
    xo_fmt_type_error = 0,
    xo_fmt_type_char, //unsigned varint
    xo_fmt_type_wchar,    //unsigned varint
    xo_fmt_type_short,    //signed varint
    xo_fmt_type_int,      //signed varint
    xo_fmt_type_long,     //signed varint
    xo_fmt_type_llong,    //unsigned varint
    xo_fmt_type_maxint,
    xo_fmt_type_ptrdiff,
    xo_fmt_type_size,
    xo_fmt_type_float,
    xo_fmt_type_double,
    xo_fmt_type_ldouble,
    xo_fmt_type_str,
    xo_fmt_type_wstr,
    xo_fmt_type_ptr,
};
xo_cassert(xo_fmt_type_ptr == 15);

struct xo_fmt_arg {
    uint16_t pos:10; //field pos
    uint16_t type:5; //arg type
    uint16_t width:1;
    uint16_t sz:10;
    uint16_t len:5;  //field spec len
    uint16_t prec:1;
};

enum { xo_fmt_max_len = (1 << 5) -1 };
enum { xo_fmt_max_str = (1 << 10) -1 };

xo_cassert(sizeof(struct xo_fmt_arg) == 4);

enum xo_fmt_tag {
    xo_fmt_tag_width = 1,
    xo_fmt_tag_prec = 2,
    xo_fmt_tag_val = 3
};

int xo_fmt_parse(const char *fmt, struct xo_fmt_arg *args, size_t nargs);

int xo_fmt_pack(uint8_t* buf, size_t sz, const char *fmt, struct xo_fmt_arg *args, size_t nargs, ...)
    __attribute__ ((format (printf, 3, 6)));

int xo_fmt_va_pack(uint8_t* buf, size_t sz, struct xo_fmt_arg *args, size_t nargs, va_list ap);

int xo_fmt_va_msg(struct xo_msg_field* msg, size_t sz, struct xo_fmt_arg *args, size_t nargs, va_list ap);

int xo_fmt_printf(char* buf, size_t sz, const char *fmt,
                  struct xo_fmt_arg *args, size_t nargs,
                  uint8_t *msg, size_t msz);
