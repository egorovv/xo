
#include "glob.h"


int xo_glob_match(const char *glob, const char *str)
{
    const char *c = str;
    const char *g = glob;
    for (;*g;) {
        if (*g == '*') {
            while (*g == '*') { ++g; }
            while (*c && *c != *g) { ++c; }
        } else if (*g == '?') {
            ++g;
            if (*c) {
                ++c;
            } else {
                return 0;
            }
        } else if (*c == *g) {
            ++c;
            ++g;
        } else {
            return 0;
        }
    }

    return *c == 0 && *g ==  0;
}
