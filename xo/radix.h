/*
 * radix.h - longest prefix match tree
 */

#pragma once

#include "xo.h"

/* internal tree node representation - app should use RADIX_TREE_ENTRY */
struct xo_radix_node {
   unsigned short    len;
   unsigned short    flags;
   struct xo_radix_node *left;
   struct xo_radix_node *right;
   struct xo_radix_node *parent;
};


struct xo_radix_tree {
   struct xo_radix_node *top;
   struct xo_radix_node  free;
   unsigned int      size;
};

/*
 * Defines a radix tree slot in the application structure it must be
 * immediately followed by the 'len' bits of the key.  The 'field' is
 * the xo_radix_node that will be passed into the radix tree funcs and
 * returned from lookup - so for casting purposes it is convenient to
 * have it first in the struct.
 */

#define XO_RADIX_TREE_ENTRY(field)                  \
   union {                                          \
      unsigned short len;                           \
      struct xo_radix_node field[2];                \
   }


/* tree init */
void xo_radix_tree_init(struct xo_radix_tree *t);

/*
 * returns best mutching entry or NULL
 */
struct xo_radix_node *
xo_radix_lookup(struct xo_radix_tree *t, void *key, unsigned int len);

/*
 * returns e or  NULL if the key exists
 */
struct xo_radix_node *
xo_radix_insert(struct xo_radix_tree *t, struct xo_radix_node *e);


/* the entry must be in the tree */
void
xo_radix_remove(struct xo_radix_tree *t, struct xo_radix_node *e);

/* returns removed entry or null */
struct xo_radix_node *
xo_radix_remove_key(struct xo_radix_tree *t, unsigned char *key, int len);


/*
 * callback gets the tree, current node and the cookie, * returns 0 to
 * continue, non-zero to stop.
 * Mutation at this point is unsafe.
 */
typedef int (*xo_radix_walk_cb)(struct xo_radix_tree *t,
                                struct xo_radix_node *x,
                                void *data);

/* in-order traversal of app entries */
struct xo_radix_node *
xo_radix_tree_walk(struct xo_radix_tree* t, xo_radix_walk_cb f, void *data);

/* in-order traversal of all nodes including internal */
struct xo_radix_node *
xo_radix_tree_walk_all(struct xo_radix_tree* t, xo_radix_walk_cb f, void *data);
