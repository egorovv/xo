#pragma once

#include "rdtsc.h"


enum {
    xo_perf_nbins = 5,
};

struct xo_perf_timer {
    uint64_t ts;
    struct xo_perf_counter *counter;
};

struct xo_perf_counter  {
    char name[23];
    uint8_t  on;
    uint64_t num;
    uint64_t sum;
    uint32_t avg;
    uint32_t cnt[xo_perf_nbins];
} __attribute__((aligned(xo_cache_line))) ;

xo_cassert(sizeof(struct xo_perf_counter) == xo_cache_line);


static inline void
xo_perf_bin(uint64_t delta, struct xo_perf_counter *cnt) {
    static uint32_t bins[xo_perf_nbins] = {30, 80, 150, 300};
    uint64_t pct;
    int bin = 0;
    pct = delta*100/cnt->avg;
    for (; bin < xo_perf_nbins - 1; ++bin) {
        if (pct < bins[bin]) {
            break;
        }
    }
    xo_atomic_add32(&cnt->cnt[bin], 1);
}


static inline void
xo_perf_add(uint64_t delta, struct xo_perf_counter *cnt) {
    xo_atomic_add64(&cnt->num, 1);
    xo_atomic_add64(&cnt->sum, delta);
    if (cnt->avg) {
        xo_perf_bin(delta, cnt);
    }
}

static inline void
xo_perf_start(struct xo_perf_timer *p, struct xo_perf_counter *cnt) {
    uint64_t ts = p->ts;

    if (cnt && !cnt->on) {
        cnt = 0;
    }

    if (cnt || p->counter) {
        p->ts = xo_rdtscp();
    }
    
    if (p->counter) {
        xo_perf_add(p->ts - ts, p->counter);
    }

    p->counter = cnt;
}


#define XO_PERF_COUNTER(name)                                           \
    static struct xo_perf_counter XO_SECTION(xo_perf_data)              \
        __xo_perf_##name = {#name};               \
    static struct xo_perf_counter XO_SECTION(xo_perf)                   \
        *__xo_perf_##name##_cnt = &__xo_perf_##name


#ifndef XO_NO_PERF

#define XO_PERF_TIMER(x) struct xo_perf_timer x = {}

#define XO_PERF_START(x, c)                     \
    xo_perf_start(&x, __xo_perf_##c##_cnt)

#define XO_PERF_STOP(x)                         \
    xo_perf_start(&x, NULL)

#define XO_PERF_INC(c, n)                       \
    xo_atomic_add64(&c->num, n);

#else

#define XO_PERF_TIMER(x)

#define XO_PERF_START(x, c)
#define XO_PERF_STOP(x)

#endif

extern struct xo_perf_counter **xo_perf_first, **xo_perf_last;


static inline size_t xo_perf_count() {
    return xo_perf_last - xo_perf_first - 1;
}

void xo_perf_dump(struct xo_perf_counter **first, size_t count, int all);

void xo_perf_init(struct xo_perf_counter *start);
void xo_perf_enable(const char * pat, int on);
//void xo_perf_dump();
void xo_perf_calibrate();
void xo_perf_clear();


