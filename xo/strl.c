#include "strl.h"

size_t
xo_strlcpy (char *dst, const char *src, size_t sz)
{
    size_t n;

    for (n = 0; n < sz; n++) {
        if ((*dst++ = *src++) == '\0') {
            break;
        }
    }

    if (n < sz) {
        return n;
    }

    if (n > 0) {
        *(dst - 1) = '\0';
    }
    return n + strlen (src);
}

size_t
xo_strlcat(char *dst, const char *src, size_t sz)
{
    char *d = dst;
    const char *s = src;
    size_t n = sz;
    size_t len;

    while (n-- != 0 && *d != '\0') {
        d++;
    }
    
    len = d - dst;
    n = sz - len;

    if (n == 0){
        return len + strlen(s);
    }

    while (*s != '\0') {
        
        if (n != 1) {
            *d++ = *s;
            n--;
        }
        s++;
    }
    *d = '\0';

    return len + (s - src);
}
