
#include "buf.h"
#include "fmt.h"
#include <stdio.h>
#include <stdarg.h>

size_t
xo_buf_printf(struct xo_buf *b, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    size_t sz = vsnprintf((char*)b->buf, b->sz, fmt, ap);
    va_end(ap);

    return xo_buf_move(b, sz);
}

size_t
xo_buf_fmt(struct xo_buf *b, const char *fmt,
           struct xo_fmt_arg *args, size_t nargs,
           uint8_t *msg, size_t msz)
{
    int n = xo_fmt_printf((char*)b->buf, b->sz, fmt, args, nargs, msg, msz);
    if (n > 0) {
        return xo_buf_move(b, n);
    }
    return 0;
}
