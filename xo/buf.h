#pragma once


#include "xo.h"

struct xo_buf {
    size_t   sz;
    uint8_t *buf;
};

static inline size_t xo_buf_has(struct xo_buf *buf, size_t sz) {
    return xo_min(buf->sz, sz);
}

static inline size_t xo_buf_sub(struct xo_buf *buf, size_t sub) {
    return buf->sz - xo_buf_has(buf, sub);
}

static inline size_t xo_buf_move(struct xo_buf *buf, size_t sz) {
    buf->buf += sz;
    buf->sz = xo_buf_sub(buf, sz);
    return sz;
}

static inline size_t xo_buf_write(struct xo_buf *buf, uint8_t *data, size_t sz) {
    memcpy(buf->buf, data, xo_buf_has(buf, sz));
    return xo_buf_move(buf, sz);
}

size_t xo_buf_printf(struct xo_buf *b, const char *fmt, ...)
        __attribute__((format (printf,2,3)));

struct xo_fmt_arg;
size_t xo_buf_fmt(struct xo_buf *b, const char *fmt,
                  struct xo_fmt_arg *args, size_t nargs,
                  uint8_t *msg, size_t msz);
