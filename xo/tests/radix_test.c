#include <xo/test.h>
#include <xo/radix.h>

//#include <stddef.h>
//#include <stdio.h>
//#include <string.h>
//#include <assert.h>

#include <arpa/inet.h>

struct route {
    const char *str;
    XO_RADIX_TREE_ENTRY(nodes);
    uint32_t   val;
};


static void parse_route(struct route *r)
{
    assert(inet_pton(AF_INET, r->str, &r->val) == 1);
}

void test_radix_tree()
{

    /* test route table */
    struct route routes[] = {
        { "0.0.0.0", {0} },
        { "10.1.0.0", {16} },
        { "10.2.0.0", {16} },
        { "10.0.0.0", {8}},
        { "192.168.0.0", {24} },
        { "192.168.1.0", {24} },
        { "172.16.0.0", {12} },
        { "10.0.0.0", {32} },
        { "10.1.0.1", {32} },
        { "192.168.0.0", {32} },
        { "192.168.1.0", {32} },
        { "172.20.1.1", {32} }
    };


    /* routes to look up and expected results */
    struct host {
        struct route r;
        struct route res;
    } hosts[] = {
        { { "10.1.0.1", {32} }, {"10.1.0.1" , {32}} },
        { { "10.2.0.1", {32} }, {"10.2.0.0" , {16}} },
        { { "10.3.0.1", {32} }, {"10.0.0.0" , {8}} },
        { { "192.168.0.0" , {32} }, {"192.168.0.0", {32} } },
        { { "192.168.0.1" , {32} }, {"192.168.0.0", {24} } },
        { { "172.20.1.1", {32} }, { "172.20.1.1", {32} }},
        { { "172.20.1.2", {32} }, { "172.16.0.0", {12} }},
        { { "172.20.1.3", {32} }, { "172.16.0.0", {12} }},
        { { "8.8.8.8" , {32} }, { "0.0.0.0", {0} }},
    };

    unsigned int count = sizeof(routes)/sizeof(routes[0]);
    unsigned int i = 0;
    struct route *def = routes;

    struct xo_radix_tree t;
   
    xo_radix_tree_init(&t);

    // poulate routes
    for (i = 0 ; i < count; ++i) {
        struct route *r = routes + i;
        parse_route(r);
        assert(xo_radix_insert(&t, r->nodes) == r->nodes);
    }

    // find routes
    for (i = 0 ; i < count; ++i) {
        struct route *r = routes + i;
        assert(xo_radix_lookup(&t, &r->val, r->len) == r->nodes);
    }

    // lookup host routes
    for (i = 0 ; i < sizeof(hosts)/sizeof(hosts[0]); ++i) {
        struct xo_radix_node* n;
        struct route *r;
        struct host *h = hosts + i;
        parse_route(&h->r);
        parse_route(&h->res);

        n = xo_radix_lookup(&t, &h->r.val, h->r.len);
        assert(n!=0);
        assert(n->len == h->res.len);
        r = (struct route *)((uint8_t*)n - offsetof(struct route, nodes));
        assert(strcmp(r->str, h->res.str) == 0);
    }


    // remove default
    xo_radix_remove(&t, def->nodes);

    // lookup routes
    for (i = 0 ; i < sizeof(hosts)/sizeof(hosts[0]); ++i) {
        struct xo_radix_node* n;
        struct host *h = hosts + i;
        parse_route(&h->r);
        parse_route(&h->res);

        n = xo_radix_lookup(&t, &h->r.val, h->r.len);

        // for hosts that resolve to defaults lookup should fail
        if (h->res.len == 0) {
            assert(n == 0);
        } else {
            struct route *r;
            assert(n !=0);
            assert(n->len == h->res.len);
            r = (struct route *)((uint8_t*)n - offsetof(struct route, nodes));
            assert(strcmp(r->str, h->res.str) == 0);
        }
    }

    // insert default back
    assert(xo_radix_insert(&t, def->nodes) == def->nodes);

    // remove
    for (i = 0 ; i < count; ++i) {
        struct route *r = routes + i;
        xo_radix_remove(&t, r->nodes);
    }

}


XO_TEST(radix_test) {
    test_radix_tree();
    return 1;
}
