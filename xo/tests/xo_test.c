#include <xo/test.h>
#include <xo/xo.h>
#include <xo/perf.h>

XO_TEST(test_cpuid) {
    int cpu = xo_getcpu();
    printf("current cpu is %d\n", cpu);
    assert(cpu >= 0);
    return 1;
}

XO_TEST(test_perf)
{
    int i;
    XO_PERF_COUNTER(test);
    XO_PERF_TIMER(tmr);


    xo_perf_clear();


    for (i = 0; i < 1000; ++i) {
        XO_PERF_START(tmr, test);
    }
    XO_PERF_STOP(tmr);

    xo_perf_dump(xo_perf_first, xo_perf_count(), 0);

    xo_perf_calibrate();

    for (i = 0; i < 1000; ++i) {
        XO_PERF_START(tmr, test);
    }
    XO_PERF_STOP(tmr);

    xo_perf_dump(xo_perf_first, xo_perf_count(), 0);
    xo_perf_clear();
    return 1;
}
