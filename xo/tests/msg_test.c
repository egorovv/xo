


#include <xo/test.h>
#include <xo/msg.h>


XO_TEST(msg_test) {

   char val[]="foobar";
   struct xo_msg_field f[] = {
      // type             tag val   len  data
      {xo_msg_type_varint, 0, 100  , 1},
      {xo_msg_type_varint, 1, 200  , 2},
      {xo_msg_type_varint, 2, 16385, 3},
      {xo_msg_type_len,    3, 0    , 6, (uint8_t*)val}
   };

   struct xo_msg_field r[xo_countof(f)] = {};

   uint8_t buf[1024];

   size_t sz1 = xo_msg_size_all(f, xo_countof(f));

   size_t sz2 = xo_msg_pack_all(buf, 1024, f, xo_countof(f));
   int n;
   int i;

   assert(sz1 == sz2);

   n = xo_msg_parse_all(buf, sz1, r, xo_countof(f));
   assert(n == xo_countof(f));

   for (i = 0; i < (int)xo_countof(f); ++i) {
      assert(f[i].type == r[i].type);
      assert(f[i].tag == r[i].tag);
      assert(f[i].len == r[i].len);
   }
   return 1;
}



XO_TEST(msg_zigzag) {

   assert(xo_msg_zigzag(1) == 2);
   assert(xo_msg_unzigzag(2) == 1);

   assert(xo_msg_zigzag(-2) == 3);
   assert(xo_msg_unzigzag(3) == -2);

   assert(xo_msg_zigzag(2147483647) == 4294967294);
   assert(xo_msg_unzigzag(4294967294) == 2147483647);

   assert(xo_msg_zigzag(-2147483648) == 4294967295);
   assert(xo_msg_unzigzag(4294967295) == -2147483648);

   return 1;
}




typedef void (*xo_msg_get_uint_fn)(void *data, uint64_t val);
typedef void (*xo_msg_get_int_fn)(void*, int64_t val);
typedef void (*xo_msg_get_bytes_fn)(void*, uint8_t *val, size_t len);



/*
message FrameHeader {
  enum Flag {
    Open = 1;
    Close = 2;
  };
  optional int32 src = 1;
  optional int32 dst = 2;
  optional Flag flags = 3;
  optional string msg = 4;
}
*/

enum FrameHeader_Flag {
   FrameHeader_Flag_Open = 1,
   FrameHeader_Flag_Close = 2,
   FrameHeader_Flag_Enum_End
};


enum FrameHeader_MsgField {
   FrameHeader_src = 1,
   FrameHeader_dst = 2,
   FrameHeader_flags = 3,
   FrameHeader_msg = 4,
};


struct FrameHeader_Parser {
    xo_msg_get_uint_fn src;
    xo_msg_get_uint_fn dst;
    xo_msg_get_int_fn flags;
    xo_msg_get_bytes_fn msg;
};


int FrameHeader_Parse(uint8_t *msg, size_t sz, struct FrameHeader_Parser* p, void *data) {
   struct xo_msg_field f;
   size_t pos = 0;
   for (pos = 0 ;pos < sz;) {
      pos += xo_msg_parse(msg + pos, sz - pos, &f);
      if (pos > sz) {
         return -1;
      }
      switch (f.tag) {
      case FrameHeader_src:
          p->src(data, f.val);
          break;
      case FrameHeader_dst:
          p->dst(data, f.val);
          break;
      case FrameHeader_flags:
          p->flags(data, xo_msg_unzigzag(f.val));
          break;
      case FrameHeader_msg:
          p->msg(data, f.data, f.len);
          break;
      default:
         break;
      }
   }
   return pos == sz;
}


struct FrameHeader_Test {
    enum FrameHeader_Flag flags;
    uint32_t len;
    uint64_t src;
    uint64_t dst;
    uint8_t msg[16];
};


#define __type_is_void(expr) __builtin_types_compatible_p(typeof(expr), void)
#define __expr_or_zero(expr) __builtin_choose_expr(__type_is_void(expr), 0, (expr))



#define DO(expr) \
    __builtin_choose_expr(__type_is_void(expr), \
        __DO_VOID(expr), \
        __DO(__expr_or_zero(expr)))


enum xo_msg_pbtype {
    xo_msg_pb_bool,     //int
    xo_msg_pb_uint32,   //uint32
    xo_msg_pb_uint64,   //uint64
    xo_msg_pb_sint32,   //int32
    xo_msg_pb_sint64,   //int64
    xo_msg_pb_fixed32,  //uint32
    xo_msg_pb_fixed64,  //uint64
    xo_msg_pb_sfixed32, //int32
    xo_msg_pb_sfixed64, //int64
    xo_msg_pb_uenum,    //uint
    xo_msg_pb_senum,    //int
    xo_msg_pb_float,    //float
    xo_msg_pb_double,   //double
    xo_msg_pb_string,   //char[]
    xo_msg_pb_bytes,    //uint8_t[]
    xo_msg_pb_message,  //uint8_t[]
};


typedef int (*xo_msg_field_cb)(struct xo_msg_field *f, void *st, void *data);

struct xo_msg_field_desc {
    uint32_t           tag;
    enum xo_msg_pbtype type;
    size_t             off;
    size_t             len;
    xo_msg_field_cb    cb;
};


struct FrameTestMsg_desc {
    size_t count;
    uint64_t mask;
    struct xo_msg_field_desc field[5];
};


#define xo_msg_field_def(n, t, st, m) \
    {n, t, offsetof(st, m), xo_field_size(st,m) }


struct xo_msg_desc {
    size_t count;
    uint64_t mask;
    struct xo_msg_field_desc field[0];
};


const struct FrameTestMsg_desc Frame_desc = {
    .count = 5,
    .field = {
        xo_msg_field_def(1, xo_msg_pb_uenum,  struct FrameHeader_Test, flags),
        xo_msg_field_def(2, xo_msg_pb_uint64, struct FrameHeader_Test, src),
        xo_msg_field_def(3, xo_msg_pb_uint64, struct FrameHeader_Test, dst),
        xo_msg_field_def(4, xo_msg_pb_uint32, struct FrameHeader_Test, len),
        xo_msg_field_def(5, xo_msg_pb_bytes,  struct FrameHeader_Test, msg)
    }
};

static void parse_test_msg_src(void* data, uint64_t val) {
   struct FrameHeader_Test *h = data;
   h->src = val;
}
static void parse_test_msg_dst(void* data, uint64_t val) {
   struct FrameHeader_Test *h = data;
   h->dst = val;
}
static void parse_test_msg_flags(void* data, int64_t val) {
   struct FrameHeader_Test *h = data;
   h->flags = val;
}

static void parse_test_msg_msg(void* data, uint8_t* val, size_t len) {
   struct FrameHeader_Test *h = data;
   h->len = len;
}


XO_TEST(msg_marshal) {

   struct FrameHeader_Parser parser = {
      .src = parse_test_msg_src,
      .dst = parse_test_msg_dst,
      .flags = parse_test_msg_flags,
      .msg = parse_test_msg_msg,
   };

   struct FrameHeader_Test src = {
      .src = 32000,
      .dst = 1234567,
      .flags = FrameHeader_Flag_Open,
      .msg = {},
      .len = 16,
   };

   struct FrameHeader_Test dst = {};
   uint8_t buffer[4096];
   size_t sz = 0;

   struct xo_msg_field msg[4] = {};
   struct xo_msg_field *f;
   int n;

   
   sz += xo_msg_pack_uint(buffer + sz, sizeof(buffer) - sz, FrameHeader_src, src.src);
   sz += xo_msg_pack_uint(buffer + sz, sizeof(buffer) - sz, FrameHeader_dst, src.dst);
   sz += xo_msg_pack_int(buffer + sz, sizeof(buffer) - sz, FrameHeader_flags, src.flags);
   sz += xo_msg_pack_bytes(buffer + sz, sizeof(buffer) - sz, FrameHeader_msg, src.msg, 16);

   assert(FrameHeader_Parse(buffer, sz, &parser, &dst));
   assert(src.src == dst.src);
   assert(src.dst == dst.dst);
   assert(src.flags == dst.flags);
   assert(src.len == dst.len);


   sz = 0;
   sz += xo_msg_pack_bytes(buffer + sz, sizeof(buffer) - sz, FrameHeader_msg, src.msg, 16);
   sz += xo_msg_pack_uint(buffer + sz, sizeof(buffer) - sz, FrameHeader_dst, src.dst);
   sz += xo_msg_pack_int(buffer + sz, sizeof(buffer) - sz, FrameHeader_flags, src.flags);
   sz += xo_msg_pack_uint(buffer + sz, sizeof(buffer) - sz, FrameHeader_src, src.src);
   
   n = xo_msg_parse_all(buffer, sz, msg, xo_countof(msg));
   assert(n == xo_countof(msg));

   f = xo_msg_find_tag(msg, xo_countof(msg), FrameHeader_src);
   assert(f  != 0);
   assert(xo_msg_get_uint(f) == src.src);

   f = xo_msg_find_tag(msg, xo_countof(msg), FrameHeader_dst);
   assert(f != 0);
   assert(xo_msg_get_uint(f) == src.dst);

   f = xo_msg_find_tag(msg, xo_countof(msg), FrameHeader_flags);
   assert(f != 0);
   assert(xo_msg_get_int(f) == src.flags);

   f = xo_msg_find_tag(msg, xo_countof(msg), FrameHeader_msg);
   assert(f != 0);
   assert(f->len == src.len);

   return 1;
}

XO_TEST(msg_find) {
   
   struct xo_msg_field msg[] = {
      // type             tag val   len  data
      {xo_msg_type_varint, 0},
      {xo_msg_type_varint, 1},
      {xo_msg_type_varint, 2},
      {xo_msg_type_len,    5},
      {xo_msg_type_len,    5},
      {xo_msg_type_len,    5},
      {xo_msg_type_len,    10}
   };


   struct xo_msg_field *f = xo_msg_find_tag(msg, xo_countof(msg), 0);
   assert(f && f->tag == 0);

   f = xo_msg_find_tag(msg, xo_countof(msg), 3);
   assert(f == 0);
   
   f = xo_msg_find_tag(msg, 3, 5);
   assert(f == 0);

   f = xo_msg_find_tag(msg, 4, 5);
   assert(f != 0 && f->tag == 5 && f - msg == 3);

   f = xo_msg_find_tag(msg, xo_countof(msg), 5);
   assert(f != 0 && f->tag == 5 && f - msg == 3);

   f = xo_msg_find_tag(msg, xo_countof(msg), 10);
   assert(f != 0 && f->tag == 10);

   f = xo_msg_find_tag(msg, xo_countof(msg), 20);
   assert(f == 0);
   
   return 1;
}
