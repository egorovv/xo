


#include <xo/fmt.h>
#include <xo/test.h>
#include <xo/buf.h>
#include <xo/msg.h>





XO_TEST(test_fmt) {


    struct xo_fmt_arg args[16];


    assert (xo_fmt_parse("%d", args, 16) == 1);
    assert(args[0].pos == 0);
    assert(args[0].len == 2);

    assert (xo_fmt_parse("hello %*.5lld world", args, 16) == 1);
    assert(args[0].pos == 6);
    assert(args[0].len == 7);
    assert(args[0].width == 1);

    assert (xo_fmt_parse("hello %1$.5lld world", args, 16) == -1);

    assert (xo_fmt_parse("hello %*.lld world", args, 16) == 1);
    assert(args[0].pos == 6);
    assert(args[0].len == 6);
    assert(args[0].width == 1);
    
    assert (xo_fmt_parse("hello %10.lld world", args, 16) == 1);
    assert(args[0].pos == 6);
    assert(args[0].len == 7);
    assert(args[0].width == 0);

    assert (xo_fmt_parse("hello %10.*llp world", args, 16) == -1);

    assert (xo_fmt_parse("hello %10.ll", args, 16) == -1);

    return 1;
}

XO_TEST(test_fmt_pack) {
    struct xo_fmt_arg args[16];
    uint8_t buf[64];
    size_t sz;
    struct xo_buf b = { 0, buf };
    int i;
    char out[128];
    int n;
    
    assert (xo_fmt_parse("one %d two %ld tree %.*s", args, 16) == 3);
    sz = xo_fmt_pack(buf, sizeof(buf), "one %d two %ld tree %.*s", args, 3,
                     10, 30L, 10, "this is a test string");

    assert(sz < sizeof(buf));

    b.buf = buf;
    b.sz = sz;
    
    for (i = 0; i< 4 ; ++i) {
        struct xo_msg_field f;
        size_t pos;
        assert(b.sz > 0);
        pos = xo_msg_parse(b.buf, b.sz, &f);
        assert(pos <= b.sz);
        xo_buf_move(&b, pos);
    }
    assert(b.sz == 0);


    n = xo_fmt_printf(out, 128, "one %d two %ld tree %.*s", args, 3, buf, sz);

    assert(n > 0);
    printf("%s\n", out);
    
    return 1;
}



static size_t test_printf(char *buf, size_t sz, const char *fmt, ...) {
    struct xo_fmt_arg args[16];
    int nargs;
    uint8_t msg[256];
    size_t msz;
    va_list ap;
    
    va_start(ap, fmt);

    nargs = xo_fmt_parse(fmt, args, 16);
    msz = xo_fmt_va_pack(msg, sizeof(msg), args, nargs, ap);

    va_end(ap);

    return xo_fmt_printf(buf, sz, fmt, args, nargs, msg, msz);
}

XO_TEST(test_fmt_print) {

    char buf1[1024], buf2[1024];

    size_t sz1, sz2;
    
    sz1 = snprintf(buf1, sizeof(buf1), "hello %d world %0X failed %.*s", 666, 0x0BAD, 4, "Oops");
    sz2 = test_printf(buf2, sizeof(buf2), "hello %d world %0X failed %.*s", 666, 0x0BAD, 4, "Oops");
    assert(sz1 == sz2);
    assert(strcmp(buf1, buf2) == 0);

    sz1 = snprintf(buf1, sizeof(buf1),"dev open %p %p %p %p %p", (void*)1, (void*)10,
                   (void*)100, (void*)101, (void*)1025);
    sz2 = test_printf(buf2, sizeof(buf2),"dev open %p %p %p %p %p", (void*)1, (void*)10,
                      (void*)100, (void*)101, (void*)1025);
    assert(sz1 == sz2);
    assert(strcmp(buf1, buf2) == 0);


    sz1 = snprintf(buf1, sizeof(buf1),"no args");
    sz2 = test_printf(buf2, sizeof(buf2),"no args");
    assert(sz1 == sz2);
    assert(strcmp(buf1, buf2) == 0);

    sz1 = snprintf(buf1, sizeof(buf1),"%d args", 1);
    sz2 = test_printf(buf2, sizeof(buf2),"%d args", 1);
    assert(sz1 == sz2);
    assert(strcmp(buf1, buf2) == 0);

    sz1 = snprintf(buf1, sizeof(buf1),"closed testdev %p", (void*)0x430ded00f1a0);
    sz2 = test_printf(buf2, sizeof(buf2),"closed testdev %p", (void*)0x430ded00f1a0);
    assert(sz1 == sz2);
    assert(strcmp(buf1, buf2) == 0);
    
    return 1;
}
