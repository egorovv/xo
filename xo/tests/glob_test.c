
#include <xo/test.h>
#include <xo/glob.h>
#include <xo/xo.h>





XO_TEST(test_glob) {

    assert(xo_glob_match("*", "abc"));
    assert(xo_glob_match("*", ""));
    assert(xo_glob_match("*x*", "abcxabc"));
    assert(xo_glob_match("???x*", "abcx"));
    assert(xo_glob_match("abc", "abc"));
    assert(xo_glob_match("abc*", "abc"));
    assert(xo_glob_match("abc?", "abcx"));
    assert(!xo_glob_match("abc?", "abc"));
    assert(!xo_glob_match("abcx", "abc"));
    assert(!xo_glob_match("abcx", "abcy"));

    return 1;
}
