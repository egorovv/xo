
#include <xo/ring.h>
#include <xo/test.h>

#define CBITS 10
#define CSZ (1<<CBITS)
#define RSZ 16


XO_TEST(test_ring)
{
   struct ring_test {
      struct xo_ring r;
      uint8_t data[CSZ*RSZ];
   } t;
   struct xo_ring *r = &t.r;
   uint8_t val[CSZ];
   int i;
   struct xo_ring_pos pos;
   uint32_t sz;

   memset(r, 0, sizeof(*r));
   
   r->size = CSZ*RSZ;
   r->chunk = CSZ;


   memset(val, 0xFF, CSZ);

   for (i = 0; i < 1024*1024; ++i) {
      xo_ring_write(r, val, i%(CSZ-4) + 1);
   }



   i = -1;
   while (xo_ring_read_start(r, &pos)) {
      void *p;
      while ((p = xo_ring_read_next(r, &pos, &sz))) {
         if (i == -1) {
            i = sz;
         }
         else {
             xo_assert(sz == (size_t)i%(CSZ-4) + 1);
            ++i;
         }
         xo_assert(memcmp(p, val, sz) == 0);
      }
      xo_ring_read_end(r, &pos);
   }
   return 1;
}

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>


const int iters = 1024;
const int nthreads = 8;
uint8_t val[CSZ];

struct test_ring_msg {
    uint32_t len;
    uint8_t data[0];
};




void * test_xo_ring_produce(void *arg) {
    struct xo_ring *r = arg;
    struct xo_ring_pos pos = {};
    struct test_ring_msg *data;
    int i = 0;
    for (; i < iters; ++i) {
        size_t sz = i%(CSZ-8) + 1;
        struct test_ring_msg *data = xo_ring_write_start(r, sizeof (struct test_ring_msg) + sz, &pos);
        data->len = sz;
        memcpy(data->data, val, sz);
        xo_ring_write_end(r, &pos);
    }
    data = xo_ring_write_start(r, sizeof (struct test_ring_msg), &pos);
    data->len = 0;
    xo_ring_write_end(r, &pos);
    return 0;
}

void * test_xo_ring_consume(void *arg) {
    struct xo_ring *r = arg;
    struct xo_ring_pos pos;
    int i = 0;
    uint32_t sz;
    int notdone = nthreads - 2;
    while (notdone) {
        struct test_ring_msg *data;
        while (!xo_ring_read_start(r, &pos)) {
            xo_yield();
        }

        while ((data = xo_ring_read_next(r, &pos, &sz))) {
            assert(sz == data->len + (sizeof (struct test_ring_msg)));
            if (data->len == 0) {
                --notdone;
            } else {
                ++i;
                assert(memcmp(data->data, val, data->len) == 0);
            }
        }
        xo_ring_read_end(r, &pos);
    }
    assert(i == (nthreads - 2)*iters);
    return 0;
}


volatile int test_xo_ring_done = 1;

void * test_xo_ring_tail(void *arg) {
    struct xo_ring *r = arg;
    struct xo_ring_pos pos = {};

    for(;!test_xo_ring_done;) {
        if (xo_ring_tail_start(r, &pos)) {
            uint32_t len;
            while (xo_ring_tail_next(r, &pos, &len)) {
            }
        } else {
            xo_yield();
        }
    }

    return 0;
}


XO_TEST(test_ring_2)
{
   pthread_t threads[nthreads];
   int i;

   struct ring_test {
      struct xo_ring r;
      uint8_t data[CSZ*RSZ];
   } t;
   struct xo_ring *r = &t.r;

   memset(r, 0, sizeof(*r));

   r->size = CSZ*RSZ;
   r->chunk = CSZ;
   r->flags = xo_ring_mode_block;


   memset(val, 0xFF, CSZ);

   test_xo_ring_done = 0;

   for (i = 0; i < (int)xo_countof(threads); ++i) {
       int rc = 0;
       if (i == (int)xo_countof(threads) - 1) {
           rc = pthread_create(&threads[i], 0, test_xo_ring_tail, r);
       } else if (i == 0) {
           rc = pthread_create(&threads[i], 0, test_xo_ring_consume, r);
       } else {
           rc = pthread_create(&threads[i], 0, test_xo_ring_produce, r);
       }
       assert(rc ==0);
   }

   for (i = 0; i < (int)xo_countof(threads); ++i) {
      void *ret;
      int rc = pthread_join(threads[i], &ret);
      assert(rc != -1 && ret == 0);
      if (i == (int)xo_countof(threads) - 2) {
          test_xo_ring_done = 1;
      }
   }

   return 1;
}



#include <sys/mman.h>
#include <signal.h>

XO_TEST(test_ring_3)
{
   pid_t threads[nthreads];
   int i;

   struct ring_test {
      struct xo_ring r;
      uint8_t data[CSZ*RSZ];
   };


   struct xo_ring *r =
       mmap(0, xo_roundup(sizeof(struct ring_test), 4096),
               PROT_READ | PROT_WRITE,
            MAP_SHARED|MAP_ANONYMOUS, -1, 0);

   memset(r, 0, sizeof(*r));

   r->size = CSZ*RSZ;
   r->chunk = CSZ;
   r->flags = xo_ring_mode_block;

   memset(val, 0xFF, CSZ);


   test_xo_ring_done = 0;

   for (i = 0; i < (int)xo_countof(threads); ++i) {
       pid_t pid = fork();
       if (pid == 0) {
           if (i == (int)xo_countof(threads) - 1) {
               test_xo_ring_tail(r);
           } else if (i == 0) {
               test_xo_ring_consume(r);
           } else {
               test_xo_ring_produce(r);
           }
           exit(0);
       }
       threads[i] = pid;
   }

   for (i = 0; i < (int)xo_countof(threads); ++i) {
      int ret;
      int rc = waitpid(threads[i], &ret, 0);
      if (i == (int)xo_countof(threads) - 2) {
          kill(threads[i+1], 9);
      }
      assert(rc == threads[i]);
      assert((i < (int)xo_countof(threads) - 1 && ret == 0) ||
             (i == (int)xo_countof(threads) - 1 && WIFSIGNALED(ret)));
   }

   return 1;
}
