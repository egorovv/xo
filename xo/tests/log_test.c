
#include <xo/log.h>
#include <xo/test.h>
#include <xo/rdtsc.h>
#include <xo/perf.h>
#include <xo/ring.h>


XO_TEST(test_log)
{
    int i;
    char rbuf[sizeof(struct xo_ring) + 4096*2];
    struct xo_ring *r = (struct xo_ring *)rbuf;
    struct xo_ring_pos pos = {};
    uint32_t sz;
    uint64_t start, stop;

    
    XO_PERF_COUNTER(log_pack);
    XO_PERF_COUNTER(log_print);
    XO_PERF_COUNTER(log);
    XO_PERF_TIMER(tmr);

    xo_perf_enable("log_pack", 1);
    xo_perf_enable("log_print", 1);

    xo_ring_init(r, 4096, 2);
    xo_log_ring = r;

    xo_perf_clear();
    start = xo_rdtsc_start();
    for(i = 0; i< 1000000; ++i) {
        XO_PERF_START(tmr, log_print);
        XO_DEBUG("[I:0x%x,L:%s]: Error: Unable to allocate Lif %d", 1000, "lif1", i);
    }
    XO_PERF_STOP(tmr);

    stop = xo_rdtsc_stop();
    printf ("log before %ld\n", (stop - start)/i);

    xo_perf_dump(xo_perf_first, xo_perf_count(), 0);
    xo_perf_clear();



    xo_log_init(0);

    start = xo_rdtsc_start();
    for(i = 0; i< 1000000; ++i) {
        XO_PERF_START(tmr, log_pack);
        XO_DEBUG("[I:0x%x,L:%s]: Error: Unable to allocate Lif %d", 1000, "lif1", i);
    }
    XO_PERF_STOP(tmr);
    
    stop = xo_rdtsc_stop();
    printf ("log after %ld\n", (stop - start)/i);

    xo_perf_dump(xo_perf_first, xo_perf_count(), 0);

    XO_LOG(XO_LOG_DEBUG, "foo %s", "bar");

    XO_LOG(XO_LOG_DEBUG, "bar %d", 100);

    while (xo_ring_read_start(r, &pos)) {
        uint8_t *p;
        while ((p = xo_ring_read_next(r, &pos, &sz))) {
            char out[256];
            xo_log_format(out, sizeof(out), p, sz, *xo_log_first, *xo_log_last);
            //printf("%s\n", out);
        }
        xo_ring_read_end(r, &pos);
    }

    return 1;
}
