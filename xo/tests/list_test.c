
#include <xo/list.h>
#include <xo/test.h>




XO_TEST(test_list){
    unsigned int i;
    struct xo_slist it[100];

    struct xo_queue q = xo_queue_initializer(q);
    
    for(i = 0; i < xo_countof(it); ++i) {
        xo_queue_push(&q, &it[i]);
    }

    for(i = 0; i < xo_countof(it); ++i) {
        struct xo_slist *l = xo_queue_pop(&q);
        assert(l - it ==  i);
    }
    return 1;
}
