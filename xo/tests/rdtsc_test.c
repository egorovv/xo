
#include <xo/test.h>
#include <xo/rdtsc.h>


XO_TEST(rdtsc_test) {

    long start, stop;
    int i;
    
    start = xo_rdtsc_start();
    for  (i = 0; i < 10000; ++i) {
        xo_rdtsc();
    }
    stop = xo_rdtsc_stop();

    printf ("rdtsc %ld\n", stop - start);

    start = xo_rdtsc_start();
    for  (i = 0; i < 10000; ++i) {
        xo_rdtscp();
    }
    stop = xo_rdtsc_stop();

    printf ("rdtscp %ld\n", stop - start);

    return 1;
}
