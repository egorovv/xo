#pragma once

#include "fmt.h"

enum xo_log_level {
    XO_LOG_PANIC = 0,
    XO_LOG_ERROR,
    XO_LOG_INFO,
    XO_LOG_DEBUG,
    XO_LOG_TRACE,
};

static inline const char *
xo_log_level_name(enum xo_log_level l) {
    const char * levels[] = {
        "PANIC",
        "ERROR",
        "INFO ",
        "DEBUG",
        "TRACE"
    };
    return levels[l];
};


#define XO_LOG_ENTRY(name, l, f)                                    \
    static struct xo_log_spec XO_SECTION(xo_log_specs) name  = {    \
        .fmt = f,                                                   \
        .file = __FILE__,                                           \
        .line = __LINE__,                                           \
        .pfunc = __FUNCTION__,                                      \
        .on = (l <= XO_LOG_DEBUG),                                  \
        .level = l,                                                 \
        .nargs = -1,                                                \
    }

#define XO_LOGF(fn, l, f, ...)                                       \
    ({                                                               \
        do {                                                         \
            XO_LOG_ENTRY(__xo_log_entry, l, f) ;                     \
            static struct xo_log_spec *__e XO_SECTION(xo_logs) =     \
                &__xo_log_entry;                                     \
            if (__e->on) {                                           \
                fn(__e, f, ##__VA_ARGS__);                           \
            }                                                        \
        } while (0);                                                 \
    })

#define XO_LOG(l, f, ...) XO_LOGF(xo_log, l, f, ##__VA_ARGS__)

#define XO_PANIC(fmt, ...) XO_LOG(XO_LOG_PANIC, fmt, ##__VA_ARGS__)
#define XO_ERROR(fmt, ...) XO_LOG(XO_LOG_ERROR, fmt, ##__VA_ARGS__)
#define XO_INFO(fmt, ... ) XO_LOG(XO_LOG_INFO,  fmt, ##__VA_ARGS__)
#define XO_DEBUG(fmt, ...) XO_LOG(XO_LOG_DEBUG, fmt, ##__VA_ARGS__)
#define XO_TRACE(fmt, ...) XO_LOG(XO_LOG_TRACE, fmt, ##__VA_ARGS__)

struct xo_log_spec {
    char fmt[512];
    char file[128];
    union {
        const char *pfunc;
        char func[128];
    };
    uint32_t count;
    int16_t line;
    int16_t id;
    int8_t on;
    int8_t level;
    int8_t nargs;
    struct xo_fmt_arg args[32];
} __attribute__((aligned(256)));


xo_cassert(sizeof(struct xo_log_spec) <= 1024);

extern struct xo_log_spec **xo_log_first;
extern struct xo_log_spec **xo_log_last;

static inline size_t xo_log_count() {
    return xo_log_last - xo_log_first;
}

void xo_log_init(struct xo_log_spec *start);

struct xo_log_spec *
xo_log_get_spec(size_t n, struct xo_log_spec *start , struct xo_log_spec *end);


extern struct xo_ring *xo_log_ring;
int xo_log(struct xo_log_spec *e, const char* fmt, ...)
    __attribute__((format (printf,2,3)));


size_t xo_log_format(char *buf, size_t sz,
                     uint8_t *msg, size_t msz,
                     struct xo_log_spec *start , struct xo_log_spec *end);

size_t xo_log_va_pack(uint8_t *buf, size_t sz, struct xo_log_spec *e, va_list ap);
