
#pragma once

#include "xo.h"

struct xo_slist {
    struct xo_slist *volatile  next;
};


void
xo_slist_push(struct xo_slist *h, struct xo_slist *l)
{
    struct xo_slist *n;
    n = h->next;
    l->next = n;
    h->next = l;
}

struct xo_slist *
xo_slist_pop(struct xo_slist *h)
{
    struct xo_slist *l;

    l = h->next;
    if (!l) {
        return 0;
    }
    h->next = l->next;
    return l;
}

struct xo_queue
{
    struct xo_slist *volatile  head;
    struct xo_slist *          tail;
    struct xo_slist            stub;
};

#define xo_queue_initializer(q)  {&q.stub, &q.stub, {0}};

static inline void
xo_queue_init(struct xo_queue *q)
{
    q->stub.next = 0;
    q->head = &q->stub;
    q->tail = &q->stub;
}

static inline void
xo_queue_push_list(struct xo_queue *q, struct xo_slist* h,
                struct xo_slist* n)
{
    struct xo_slist* prev;
    n->next = 0;
    prev = xo_xchg(&q->head, n);
    prev->next = h;
}

static inline void
xo_queue_push(struct xo_queue *q, struct xo_slist* n)
{
    xo_queue_push_list(q, n, n);
}


static inline struct xo_slist*
xo_queue_pop(struct xo_queue *q)
{
    struct xo_slist *tail = q->tail;
    struct xo_slist *next = tail->next;
    if (tail == &q->stub) {
        if (next == 0) {
            return 0;
        }
        q->tail = tail = next;
        next = tail->next;
    }
    if (next) {
        q->tail = next;
        return tail;
    }

    if (tail != q->head) {
        // temporary broken chain
        return 0;
    }

    // single node at end of the chain
    xo_queue_push(q, &q->stub);
    
    next = tail->next;
    if (next)  {
        q->tail = next;
        return tail;
    }
    return 0;
}


struct xo_list {
    struct xo_list *prev;
    struct xo_list *next;
};

/* link list init */
static void
xo_list_init(struct xo_list *n)
{
   n->prev = n->next = n;
}

/* link list is empty */
static inline int
xo_list_empty(struct xo_list *n)
{
   return (!n->prev && !n->next) || (n->next == n);
}

/* link list insert */
static void
xo_list_link(struct xo_list *l, struct xo_list *n)
{
   xo_assert(xo_list_empty(n));
   if (l->prev == 0) { // zero initialized list
      xo_list_init(l);
   }
   n->prev = l;
   n->next = l->prev;

   l->prev->next = n;
   l->prev = n;
   xo_assert(!xo_list_empty(l));
   xo_assert(!xo_list_empty(n));
}

/* link list remove */
static void
xo_list_unlink(struct xo_list *n)
{
   xo_assert(!xo_list_empty(n));

   n->prev->next = n->prev;
   n->prev->next = n->next;

   xo_list_init(n);
}

