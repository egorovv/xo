#pragma once

#include "xo.h"

/*
 * ring is split split into chinks
 * every record  fits entirely into chunk
 * record - uint32 len + len bytes  + len % 4 padding
 */




typedef struct xo_ring_end {
    volatile uint32_t head;
    volatile uint32_t tail;
} xo_ring_end;


enum xo_ring_mode {
    xo_ring_mode_drop = 0,
    xo_ring_mode_fail,
    xo_ring_mode_block
};

struct xo_ring {
    uint32_t size;
    uint16_t chunk;
    uint8_t  flags;
    __attribute__((aligned(xo_cache_line))) xo_ring_end prod;
    __attribute__((aligned(xo_cache_line))) xo_ring_end cons;
    __attribute__((aligned(xo_cache_line))) uint8_t data[0];
};


struct xo_ring_pos {
    uint32_t head;
    uint32_t tail;
    uint32_t pos;
};

#define xo_ring_size(chunk, count) sizeof(struct xo_ring) + (chunk) * (count)


static inline void xo_ring_init(struct xo_ring *r, size_t chunk, size_t count) {
    xo_ring_end e = {};
    r->size = chunk*count;
    r->chunk = chunk;
    r->flags = 0;
    r->prod = r->cons = e;
}


static inline size_t xo_ring_avail(struct xo_ring *r) {
    return (r->prod.tail - r->cons.head) % r->size;
}

static inline size_t xo_ring_free(struct xo_ring *r) {
    return (r->size - 1 + r->cons.tail - r->prod.head) % r->size;
}

static inline uint8_t*
xo_ring_read_start(struct xo_ring *r, struct xo_ring_pos* pos)
{
    uint32_t cons_head, cons_next, prod_tail;
    uint32_t avail;

    do {
        prod_tail = r->prod.tail;
        cons_head = r->cons.head;
        avail = (prod_tail - cons_head) % r->size;
        // up to chunk end or prod
        avail = xo_min(avail, r->chunk - cons_head % r->chunk);

        if (avail == 0) {
            return 0;
        }

        cons_next = (cons_head + avail) % r->size;

    } while (xo_atomic_cmpset32(&r->cons.head, &cons_head, cons_next) == 0);

    pos->tail = cons_head;
    pos->head = cons_head + avail;
    pos->pos = cons_head ;
    return &r->data[pos->pos];
}

static inline void*
xo_ring_read_next(struct xo_ring *r, struct xo_ring_pos* p, uint32_t *len)
{
    uint32_t pos = p->pos;
    if (p->pos == p->head) {
        return 0;
    }

    memcpy(len, &r->data[pos], sizeof(uint32_t));

    if (*len == 0) {
        // chunk tail
        return 0;
    }

    p->pos += xo_roundup(*len, sizeof(uint32_t)) + sizeof(uint32_t);
    return &r->data[pos + sizeof(uint32_t)];
}


static inline void
xo_ring_read_end(struct xo_ring *r, struct xo_ring_pos* pos)
{
    xo_assert(pos->tail != pos->head);

    xo_rmb();

    while (r->cons.tail != pos->tail)
        xo_pause();

    r->cons.tail = pos->head % r->size;
    pos->tail = pos->head;
}


static inline uint8_t*
xo_ring_tail_start(struct xo_ring *r, struct xo_ring_pos* pos)
{
    uint32_t cons_head, prod_tail;
    uint32_t avail;
    uint32_t chunk = r->chunk;

    prod_tail = r->prod.tail;
    if (pos->head) {
        cons_head = pos->head % r->size;
    } else {
        cons_head = r->cons.head;
    }


    avail = (prod_tail - cons_head) % r->size ;
    // up to chunk end or prod
    avail = xo_min(avail, chunk - cons_head % chunk);

    if (avail == 0) {
        return 0;
    }

    pos->tail = cons_head;
    pos->head = cons_head + avail;
    pos->pos = cons_head ;
    return &r->data[pos->pos];
}

static inline uint8_t*
xo_ring_tail_next(struct xo_ring *r, struct xo_ring_pos* p, uint32_t *len)
{
    uint8_t *data;
    size_t sz;
    for (;;) {
        if (p->pos >= p->head && xo_ring_tail_start(r, p) == 0) {
            return 0;
        }

        memcpy(len, &r->data[p->pos], sizeof(uint32_t));

        if (*len == 0) {
            p->pos = p->head;
            continue;
        }

        sz = xo_roundup(*len, sizeof(uint32_t)) + sizeof(uint32_t);
        if (p->pos + sz > p->head) {
            // bad state - restart
            return 0;
        }
        data = r->data + p->pos + sizeof(uint32_t);
        p->pos += sz;
        return data;
    }
    return 0;
}


static inline void
xo_ring_drop(struct xo_ring *r)
{
    uint32_t cons_head, cons_next, prod_tail;
    uint32_t avail;

    prod_tail = r->prod.tail;
    cons_head = r->cons.head;
    avail = (prod_tail - cons_head) % r->size;
    // up to chunk end or prod
    avail = xo_min(avail, r->chunk - cons_head % r->chunk);

    if (avail == 0) {
        return ;
    }

    cons_next = (cons_head + avail) % r->size;

    if (xo_atomic_cmpset32(&r->cons.head, &cons_head, cons_next) == 0) {
        return;
    }

    while (r->cons.tail != cons_head) {
        xo_yield();
    }

    r->cons.tail = cons_next;
}

static inline uint32_t
xo_ring_clear(struct xo_ring *r)
{
    uint32_t cons_head, cons_next, prod_tail;
    uint32_t avail;

    prod_tail = r->prod.tail;
    cons_head = r->cons.head;
    avail = (prod_tail - cons_head) % r->size;

    if (avail == 0) {
        return 0;
    }

    cons_next = (cons_head + avail) % r->size;

    if (xo_atomic_cmpset32(&r->cons.head, &cons_head, cons_next) == 0) {
        return 0;
    }

    while (r->cons.tail != cons_head) {
        xo_yield();
    }

    r->cons.tail = cons_next;
    return avail;
}



static inline int
xo_ring_write(struct xo_ring *r, void *buf, uint32_t sz)
{
    uint32_t prod_head, prod_next;
    uint32_t cons_tail;
    uint32_t chunk = r->chunk;
    uint32_t tail;
    uint32_t pos;

    size_t len = sizeof(uint32_t) + xo_roundup(sz, sizeof(uint32_t));
    xo_assert(len <= chunk);

    for (;;) {
        uint32_t avail;
        prod_head = r->prod.head;
        cons_tail = r->cons.tail;

        // the reminder of the chunk
        tail = xo_roundup(prod_head + 1, chunk) - prod_head;
        if (tail >= len) {
            tail = 0;
        }

        avail = (r->size - 1 + cons_tail - prod_head) % r->size;
        if (avail < len + tail) {
            switch (r->flags) {
            case xo_ring_mode_drop:
                xo_ring_drop(r);
                break;
            case xo_ring_mode_fail:
                return -1;
            case xo_ring_mode_block:
                xo_yield();
                break;
            }
            continue;
        }

        prod_next = (prod_head + len + tail) % r->size;

        if (xo_atomic_cmpset32(&r->prod.head, &prod_head, prod_next)) {
            break;
        }
    }

    pos = prod_head;
    // empty record
    if (tail) {
        // zero chunk tail
        memset(&r->data[pos], 0 , tail);
        pos = (pos + tail) % r->size ;
    }

    memcpy(&r->data[pos], &sz , sizeof(uint32_t));
    memcpy(&r->data[pos + sizeof(uint32_t)], buf, sz);
    // zero tail padding
    memset(&r->data[pos + sizeof(uint32_t) + sz], 0, len - sizeof(uint32_t) - sz);

    xo_wmb();

    while (r->prod.tail != prod_head) {
        xo_pause();
    }

    r->prod.tail = prod_next;
    return 0;
}


static inline void*
xo_ring_write_start(struct xo_ring *r, size_t sz, struct xo_ring_pos *p)
{
    uint32_t prod_head, prod_next;
    uint32_t cons_tail;
    uint32_t tail;
    uint32_t chunk = r->chunk;
    size_t len = sizeof(uint32_t) + xo_roundup(sz, sizeof(uint32_t));
    uint32_t pos;
    
    xo_assert(len <= chunk);

    for (;;) {
        uint32_t avail;
        prod_head = r->prod.head;
        cons_tail = r->cons.tail;

        // the reminder of the chunk
        tail = xo_roundup(prod_head + 1, chunk) - prod_head;
        if (tail >= len) {
            tail = 0;
        }

        avail = (r->size - 1 + cons_tail - prod_head) % r->size;
        if (avail < len + tail) {
            switch (r->flags) {
            case xo_ring_mode_drop:
                xo_ring_drop(r);
                break;
            case xo_ring_mode_fail:
                return 0;
            case xo_ring_mode_block:
                xo_yield();
                break;
            }
            continue;
        }

        prod_next = (prod_head + len + tail) % r->size;

        if (xo_atomic_cmpset32(&r->prod.head, &prod_head, prod_next)) {
            break;
        }

    }

    pos = prod_head;
    // empty record
    if (tail) {
        // zero chunk tail
        memset(&r->data[pos], 0 , tail);
        pos = (pos + tail) % r->size ;
    }

    memcpy(&r->data[pos], &sz , sizeof(uint32_t));
    // zero tail padding
    memset(&r->data[pos + sizeof(uint32_t) + sz], 0, len - sizeof(uint32_t) - sz);

    p->tail = prod_head;
    p->head = prod_next;
    return &r->data[pos + sizeof(uint32_t)];
}

static inline void
xo_ring_write_end(struct xo_ring *r, struct xo_ring_pos *p)
{
    xo_wmb();

    while (r->prod.tail != p->tail) {
        xo_pause();
    }

    r->prod.tail = p->head;
}
