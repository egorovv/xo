#include "test.h"
#include "xo.h"
#include "stdarg.h"
#include "glob.h"


XO_SECTION_STARTEND(struct xo_test_def, xo_test, \
                    xo_test_def_first, xo_test_def_last);

static int
xo_test_match(const char *name, int argc, char **argv)
{
    int i = 0;
    if (argc < 1) {
        return 1;
    } else {
        for (i = 0; i < argc; ++i) {
            if (xo_glob_match(argv[i], name)) {
                return 1;
            }
        }
        return 0;
    }
}

int xo_test_run(int argc, char **argv)
{
    struct xo_test_def *t = xo_test_def_first;
    for (; t != xo_test_def_last; ++t) {

        if (xo_test_match(t->name, argc, argv) == 0) {
            printf("skipping test %s\n", t->name);
            continue;
        }

        printf("running test %s ... ", t->name);
        fflush(stdout);

        if (t->fn()) {
            printf("ok\n");
        } else {
            printf("failed\n");
        }
    }
    return 0;
}



int main(int argc, char **argv) {
    return xo_test_run(argc-1, argv+1);
}

