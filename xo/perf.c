
#include "perf.h"
#include "glob.h"

XO_SECTION_STARTEND(struct xo_perf_counter *, xo_perf,
                    xo_perf_first, xo_perf_last);
XO_SECTION_STARTEND(struct xo_perf_counter, xo_perf_data,
                    xo_perf_data_first, xo_perf_data_last);

XO_PERF_COUNTER(xo_perf_dummy);

void xo_perf_dump(struct xo_perf_counter **first, size_t count, int all) {
    struct xo_perf_counter **it = first;
    struct xo_perf_counter **last = first + count;
    printf("\n");
    while (it != last) {
        struct xo_perf_counter *c = *it;
        uint32_t num = c->num;
        if (all || num) {
            uint32_t avg = c->avg;
            long a = num ? c->sum/num : 0;
            printf("%s\t%d\t%ld\t%ld", c->name, num, (long)c->sum, a);
            if (avg) {
                int j = 0;
                printf("\t%d *", avg);
                for (; j < xo_perf_nbins; ++j) {
                    printf("\t%d", c->cnt[j]);
                }
            }
            printf("\n");
        }
        ++it;
    }
}

void xo_perf_calibrate() {
    struct xo_perf_counter **it = xo_perf_first;
    while (it != xo_perf_last) {
        struct xo_perf_counter *c = *it;
        uint32_t num = c->num;
        if (num) {
            int j = 0; 
            c->avg = c->sum/num;
            c->num = 0;
            c->sum = 0;
            for (;j < xo_perf_nbins; ++j) {
                c->cnt[j] = 0;
            }
        }
        ++it;
    }
}

void xo_perf_clear() {
    struct xo_perf_counter **it = xo_perf_first;
    while (it != xo_perf_last) {
        struct xo_perf_counter *c = *it;
        int j = 0;
        c->avg = 0;
        c->num = 0;
        c->sum = 0;
        for (; j < xo_perf_nbins; ++j) {
            c->cnt[j] = 0;
        }
        ++it;
    }
}

void xo_perf_enable(const char *pat, int on) {
    struct xo_perf_counter **it = xo_perf_first;
    while (it != xo_perf_last) {
        struct xo_perf_counter *c = *it;
        if (!pat || xo_glob_match(pat, c->name)) {
            c->on = on;
        }
        ++it;
    }
}



void xo_perf_init(struct xo_perf_counter *start)
{
    struct xo_perf_counter **it = xo_perf_first;
    while (it != xo_perf_last) {
        struct xo_perf_counter *c = *it;
        if (c) {
            *start = *c;
            *it = start;
        }
        ++start;
        ++it;
    }
}
