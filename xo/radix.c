/* **********************************************************
 * Copyright 2016 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 * **********************************************************/

/*
 * Application provides two tree nodes - one to hold the key and one
 * to build the tree if necessary (aux). Aux nodes are used if two
 * nodes have a prefix which is not a key - 'bar' and 'baz' have a
 * prefix 'ba'. Unused aux nodes sit in a freelist attached to the
 * tree. When 'ba' key is inserted it will replace the aux node and
 * free it into the list. If 'ba' key is removed it will be replaced
 * by a free aux node.  When entry is removed and its aux node is in
 * use it is replaced by a free one.
 */

#include "radix.h"

// aux node flag bits
enum {
   RADIX_NODE_AUX  = 0x1,
   RADIX_NODE_FREE = 0x2,
};


/*
 * free list made out of aux nodes linked with left/right pointers
 */

/* link list init */
static void
xo_radix_list_init(struct xo_radix_node *n)
{
   n->left = n->right = n;
}

/* link list is empty */
static inline int
xo_radix_list_empty(struct xo_radix_node *n)
{
   return (!n->right && !n->left) || (n->right == n);
}

/* link list insert */
static void
xo_radix_list_link(struct xo_radix_node *l, struct xo_radix_node *n)
{
   xo_assert(xo_radix_list_empty(n));
   if (l->right == 0) { // zero initialized list
      xo_radix_list_init(l);
   }
   n->right = l;
   n->left = l->left;

   l->left->right = n;
   l->left = n;
   xo_assert(!xo_radix_list_empty(l));
   xo_assert(!xo_radix_list_empty(n));
}

/* link list remove */
static void
xo_radix_list_unlink(struct xo_radix_node *n)
{
   xo_assert(!xo_radix_list_empty(n));

   n->left->right = n->right;
   n->right->left = n->left;

   xo_radix_list_init(n);
}

/* key immediately follows the aux node */
static inline unsigned char *
xo_radix_node_key(struct xo_radix_node *x)
{
   xo_assert(x->flags == 0);
   return (unsigned char*)(x + 2);
}

/* get a bit at offset */
static inline int
xo_radix_bit(unsigned char *x, unsigned int off)
{
   return x[off / 8] & (0x80 >> (off % 8));
}

/*
 * compares bytes up to len bits starting from off.
 * returns zero if equal
 */
static int
xo_radix_diff_bytes(unsigned char *x, unsigned char *y,
               unsigned int len, int *off)
{
   unsigned int pos = off ? (*off/8)*8 : 0;
   unsigned char diff = 0;

   for (; pos < len && diff == 0 ; pos += 8) {
      diff = x[pos/8] ^ y[pos/8];
   }

   if (pos > len) {
      diff >>= (pos - len);
      pos = len;
   }

   if (off) {
      *off = pos;
      xo_assert(*off >= 0);
   }

   return diff;
}

/*
 * returns number of same bits up to len.
 * assumes that starting off bits are the same.
 */
static int
xo_radix_diff_bits(unsigned char *x, unsigned char *y,
              unsigned int len, int off)
{
   int diff = xo_radix_diff_bytes(x, y, len, &off);

   for (; diff; --off) {
      diff >>= 1;
   }

   xo_assert(off >= 0);
   return off;
}


/* sanity check */
static int
xo_radix_node_check(struct xo_radix_node *x)
{
   if (!x) {
      return 1;
   }

   if (x->parent) {
      xo_assert(x->parent->left == x || x->parent->right == x);
      xo_assert(x->parent != x);
      xo_assert(x->parent->len < x->len);
   }

   if (x->left) {
      xo_assert(x->left->parent == x);
      xo_assert(x->left != x);
      xo_assert(x->left->len > x->len);
   }

   if (x->right) {
      xo_assert(x->right->parent == x);
      xo_assert(x->right != x);
      xo_assert(x->right->len > x->len);
   }

   if (x->flags) {
      xo_assert(x->flags == RADIX_NODE_AUX);
      xo_assert((x - 1)->flags == 0);
      xo_assert(x->left && x->right);
   } else {
      xo_assert((x + 1)->flags & RADIX_NODE_AUX);
   }

   return 1;
}

/* get a free node */
static struct xo_radix_node*
xo_radix_free_get(struct xo_radix_tree *t)
{
   struct xo_radix_node *n = (struct xo_radix_node*)t->free.left;
   xo_radix_list_unlink(t->free.left);
   xo_assert(n->flags == (RADIX_NODE_AUX|RADIX_NODE_FREE));
   n->flags &= ~RADIX_NODE_FREE;
   return n;
}

/* pool a free node */
static void
xo_radix_free_put(struct xo_radix_tree *t, struct xo_radix_node *n)
{
   xo_assert(n->flags == RADIX_NODE_AUX);
   n->flags |= RADIX_NODE_FREE;
   xo_radix_list_init(n);
   xo_radix_list_link(&t->free, n);
}


/* returns the parent's pointer pointing to a given node */
static struct xo_radix_node **
xo_radix_parent_p(struct xo_radix_tree *t, struct xo_radix_node *n)
{
   struct xo_radix_node **pp;

   if (!n->parent) {
      pp = &t->top;
   } else if (n->parent->left == n) {
      pp = &n->parent->left;
   } else {
      pp = &n->parent->right;
   }
   return pp;
}

/* plant node x in place of node n */
static void
xo_radix_graft(struct xo_radix_tree *t, struct xo_radix_node *n,
               struct xo_radix_node *x)
{
   struct xo_radix_node **pp = xo_radix_parent_p(t, n);
   xo_assert(n != x);

   *pp = x;

   if (x) {
      x->parent = n->parent;
      xo_assert(xo_radix_node_check(x));
   }
}

/*
 * returns the best matching key node, *last will be filled with the
 * node that stopped the search
 */

static struct xo_radix_node*
xo_radix_search(struct xo_radix_tree *t,
                unsigned char *key, int len,
                struct xo_radix_node **last)
{
   struct xo_radix_node *x = t->top;
   struct xo_radix_node *p = 0;
   struct xo_radix_node *best = 0;
   int off = 0;

   while (x) {
      p = x;

      if (!x->flags) {
         /* key node - match the key */
         if (len < x->len) {
            break;
         }

         if (xo_radix_diff_bytes(xo_radix_node_key(x), key, x->len, &off) != 0) {
            break;
         }

         best = x;
         off = x->len;

         if (x->len == len) {
            break;
         }

      }

      if (xo_radix_bit(key, x->len)) {
         x = x->right;
      } else {
         x = x->left;
      }
   }

   if (last) {
      *last = p;
   }
   return best;
}

/*
 *----------------------------------------------------------------------------
 *
 * xo_radix_lookup --
 *
 *    Get the longest matching prefix entry.
 *
 *----------------------------------------------------------------------------
 */
struct xo_radix_node*
xo_radix_lookup(struct xo_radix_tree *t,
                void *key, unsigned int len)
{
   return xo_radix_search(t, key, len, 0);
}


/*
 *----------------------------------------------------------------------------
 *
 * RadixInsert --
 *
 *    Insert new entry into the radix tree
 *
 *----------------------------------------------------------------------------
 */
struct xo_radix_node*
xo_radix_insert(struct xo_radix_tree *t, struct xo_radix_node nodes[2])
{
   struct xo_radix_node *n = nodes, *nn = n + 1;
   struct xo_radix_node *x = t->top;
   struct xo_radix_node *best = 0;
   struct xo_radix_node **pp;
   unsigned char *key = xo_radix_node_key(n);
   int bit;
   unsigned char *xkey;

   best = xo_radix_search(t, key, n->len, &x);

   // check for exact match
   if (best && best->len == n->len) {
      return 0;
   }

   n->flags = 0;
   nn->flags = RADIX_NODE_AUX;
   n->left = n->right = n->parent = 0;
   xo_radix_free_put(t, nn);


   if (!x) {
      n->parent = 0;
      t->top = n;
      goto done;
   }

   xkey = xo_radix_node_key(x);
   bit = x->len > n->len ? n->len : x->len;
   bit = xo_radix_diff_bits(xkey, key, bit, 0);

   // go up to finde the insertiopn point
   while (x->parent && x->parent->len >= bit) {
      x = x->parent;
   }

   pp = xo_radix_parent_p(t, x);

   // split at bit and insert aux node
   if (bit < x->len && bit < n->len) {
      nn = xo_radix_free_get(t);
      xo_assert(nn == n + 1);
      nn->len = bit;
      nn->parent = x->parent;
      x->parent = nn;
      n->parent = nn;
      *pp = nn;
      if (xo_radix_bit(key, bit)) {
         nn->right = n;
         nn->left = x;
      } else {
         nn->right = x;
         nn->left = n;
      }
   } else if (bit == x->len && x->flags) { // replace aux node
      xo_assert(x->len == n->len);
      xo_assert(x->left != 0);
      xo_assert(x->right != 0);

      n->left = x->left;
      n->right = x->right;
      n->parent = x->parent;
      n->right->parent = n->left->parent = n;
      *pp = n;
      xo_radix_free_put(t, x);
   } else if (bit == x->len) { // insert child node
      xo_assert(n->len > x->len);
      n->parent = x;
      if (xo_radix_bit(key, bit)){
         x->right = n;
      } else {
         x->left = n;
      }
   } else { //insert before
      xo_assert(n->len < x->len);
      n->parent = x->parent;
      x->parent = n;
      if (xo_radix_bit(xkey, bit)) {
         n->right = x;
      } else {
         n->left = x;
      }
      *pp = n;
   }

done:
   xo_assert(xo_radix_node_check(n));
   t->size += 1;
   return n;
}


/*
 *----------------------------------------------------------------------------
 *
 * xo_radix_remove --
 *
 *    Remove an entry from the radix tree
 *
 *----------------------------------------------------------------------------
 */
void
xo_radix_remove(struct xo_radix_tree *t, struct xo_radix_node nodes[2])
{
   struct xo_radix_node *x = nodes;
   struct xo_radix_node *p = x->parent;
   struct xo_radix_node *n ;

   xo_assert(t->size > 0);
   xo_assert(xo_radix_node_check(x));

   if (x->left && x->right) {
      // replace with aux
      n = xo_radix_free_get(t);
      n->left = x->left;
      n->right = x->right;
      n->parent = x->parent;
      n->len = x->len;
      x->left->parent = x->right->parent = n;
      xo_radix_graft(t, x, n);
   } else if (!x->left && !x->right) {
      // leaf node - remove aux parent
      if(p && p->flags) {
         xo_assert(xo_radix_node_check(p));
         n = (p->left == x) ? p->right : p->left;
         xo_radix_graft(t, p, n);
         xo_radix_free_put(t, p);
         xo_assert(xo_radix_node_check(n->parent));
      }
      else {
         xo_radix_graft(t, x, 0);
      }
   } else {
      // one leg - reparent
      n = x->left;
      if (!n) {
         n = x->right;
      }
      xo_radix_graft(t, x, n);
   }

   // remove aux node
   n = x + 1;
   if (n->flags == RADIX_NODE_AUX) {
      // aux node is in use - replace it
      struct xo_radix_node *nn = xo_radix_free_get(t);
      *nn = *n;
      if (nn->right) {
         nn->right->parent = nn;
      }
      if (nn->left) {
         nn->left->parent = nn;
      }
      xo_radix_graft(t, n, nn);
   } else {
      // must be in the free list
      xo_radix_list_unlink(n);
   }

   //make node invalid to catch double remove
   n->flags = 0;
   t->size -= 1;
}

/*
 *----------------------------------------------------------------------------
 *
 * xo_radix_remove_key --
 *
 *    Remove an entry matching a given key from the radix tree
 *
 *----------------------------------------------------------------------------
 */
struct xo_radix_node *
xo_radix_remove_key(struct xo_radix_tree *t, unsigned char *key, int len)
{
   struct xo_radix_node *x = xo_radix_search(t, key, len, 0);

   /* key prefix node */
   if (x && x->len == len) {
      xo_radix_remove(t, x);
      return x;
   } else {
      return 0;
   }
}


/*
 *----------------------------------------------------------------------------
 *
 * xo_radix_tree_init --
 *
 *    Init radix tree structure
 *
 *----------------------------------------------------------------------------
 */
void
xo_radix_tree_init(struct xo_radix_tree *t)
{
   t->top = 0;
   xo_radix_list_init(&t->free);
}

/*
 *----------------------------------------------------------------------------
 *
 * xo_radix_tree_walk_all --
 *
 *    In-order walk of all nodes including auxiliary
 *
 *----------------------------------------------------------------------------
 */
struct xo_radix_node *
xo_radix_tree_walk_all(struct xo_radix_tree* t, xo_radix_walk_cb f, void *data)
{
   struct xo_radix_node *x = t->top;

   while (x && x->left) {
      x = x->left;
   }

   while (x) {

      xo_assert(xo_radix_node_check(x));

      if (f(t, x, data)) {
         break;
      }

      if (x->right) {
         x = x->right;
         while (x->left) {
            x = x->left;
         }
      } else {
         struct xo_radix_node *p = x;
         do {
            p = x;
            x = x->parent;
         } while (x && x->right == p);
      }
   }
   return x;
}



struct xo_radix_walk_data {
   xo_radix_walk_cb f;
   void            *data;
};

static int
xo_radix_tree_walk_all_cb(struct xo_radix_tree *t,
                          struct xo_radix_node *x,
                          void *data)
{
   struct xo_radix_walk_data *w =  data;
   if (x->flags) {
      return 0;
   } else {
      return w->f(t, x, w->data);
   }
}

/*
 *----------------------------------------------------------------------------
 *
 * xo_radix_tree_walk --
 *
 *    In-order walk of application key entries
 *
 *----------------------------------------------------------------------------
 */
struct xo_radix_node *
xo_radix_tree_walk(struct xo_radix_tree* t, xo_radix_walk_cb f, void *data)
{
   struct xo_radix_walk_data w = { f, data };
   return xo_radix_tree_walk_all(t, xo_radix_tree_walk_all_cb, &w);
}
