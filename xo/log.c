#include "log.h"
#include "buf.h"
#include "msg.h"
#include "ring.h"
#include "rdtsc.h"
#include "perf.h"
#include "strl.h"


XO_SECTION_STARTEND(struct xo_log_spec *, xo_logs, xo_log_first, xo_log_last);
XO_SECTION_STARTEND(struct xo_log_spec, xo_log_specs, xo_log_spec_first, xo_log_spec_last);

struct xo_ring * xo_log_ring = 0;
int xo_log_level = XO_LOG_TRACE;

static void __attribute__((used)) xo_log_dummy()  {
    XO_DEBUG("dummy");
}

void xo_log_init(struct xo_log_spec *start)
{
    struct xo_log_spec **spec = xo_log_first;
    struct xo_log_spec *lspec = xo_log_spec_first;
    int id = 0;
    while (spec != xo_log_last) {
        struct xo_log_spec *e = *spec;
        if (!e) {
            ++spec;
            ++lspec;
            continue;
        }
        e->nargs = xo_fmt_parse(e->fmt, e->args, xo_countof(e->args));
        if (e->nargs > (int)xo_countof(e->args)) {
            e->nargs = -1;
        }
        e->id = id++;
        if (start) {
            *start = *e;
            xo_strlcpy(start->func, e->pfunc, sizeof(start->func));
            *spec = start;
            ++start;
        }
        ++spec;
        ++lspec;
    }
}



enum xo_log_tag {
    xo_log_tag_id = xo_fmt_tag_val + 1, //uint
    xo_log_tag_cpu,                     //int
    xo_log_tag_ts,                      //uint64
    xo_log_tag_head,
    xo_log_tag_msg,
};

struct xo_log_head  {
    uint64_t ts;
    uint16_t id;
    uint8_t  cpu;
    uint8_t  packed;
};


static void
xo_log_pack_head(struct xo_buf *b, struct xo_log_spec *e,
                 struct xo_log_head *h)
{
    xo_buf_move(b, xo_msg_pack_uint(b->buf, b->sz, xo_log_tag_id, h->id));
    xo_buf_move(b, xo_msg_pack_ufixed(b->buf, b->sz, xo_log_tag_ts, h->ts));
    xo_buf_move(b, xo_msg_pack_int(b->buf, b->sz, xo_log_tag_cpu, h->cpu));
}

size_t
xo_log_va_pack(uint8_t *buf, size_t sz, struct xo_log_spec *e, va_list ap)
{
    struct xo_buf b = { sz, buf};


    if (e->nargs == -1) {
        xo_buf_move(&b, vsnprintf((char*)b.buf, b.sz, e->fmt, ap));
    }  else {
        xo_buf_move(&b, xo_fmt_va_pack(b.buf, b.sz, e->args, e->nargs, ap));
    }

    return b.buf - buf;
}

int xo_log(struct xo_log_spec *e, const char *fmt, ...) {
    va_list ap;
    size_t sz;
    uint8_t buf[4096];
    struct xo_buf b = { sizeof(buf), buf};

    struct xo_log_head  h  = {};

    xo_atomic_add32(&e->count, 1);

    h.id = e->id;
    h.ts = xo_rdtsc();
    h.cpu = xo_getcpu();
    h.packed = (e->nargs == -1) ? 0 : 1;

    xo_log_pack_head(&b, e, &h);

    va_start(ap, fmt);
    sz = xo_log_va_pack(b.buf, b.sz, e, ap);
    va_end(ap);

    if(sz < sizeof(buf) && xo_log_ring != 0) {
        xo_ring_write(xo_log_ring, b.buf, sz);
    }

    return sz;
}

struct xo_log_spec *
xo_log_get_spec(size_t n, struct xo_log_spec *start , struct xo_log_spec *end)
{
    if ((ptrdiff_t)n > end - start) {
        return 0;
    } else {
        return &start[n];
    }
}


size_t xo_log_format(char *buf, size_t sz, uint8_t *msg, size_t msz,
                     struct xo_log_spec *start , struct xo_log_spec *end)
{
    size_t i;
    size_t fsz;
    struct xo_msg_field m[3];
    struct xo_msg_field *f;
    struct xo_log_spec *e;

    struct xo_buf b = {msz, msg};
    struct xo_buf out = { sz, (uint8_t*)buf};
    struct xo_log_head h = {};


    for (i = 0; i < xo_countof(m); ++i) {
        f = m + i;
        xo_buf_move(&b, xo_msg_parse(b.buf, b.sz, f));
        if (b.buf > msg + msz) {
            return -1;
        }

        switch (i) {
        case 0:
            if (f->tag != xo_log_tag_id) {
                return -1;
            }
            h.id = xo_msg_get_uint(f);
            break;
        case 1:
            if (f->tag != xo_log_tag_ts) {
                return -1;
            }
            h.ts = xo_msg_get_ufixed(f);
            break;
        case 2:
            if (f->tag != xo_log_tag_cpu) {
                return -1;
            }
            h.cpu = xo_msg_get_int(f);
            break;
        default:
            break;
        }
    }

    e = xo_log_get_spec(h.id, start, end);
    if (e == 0) {
        return -1;
    }
    //parsed
    if (e->nargs > 0) {
        f = m;
        fsz = xo_msg_parse(b.buf, b.sz, f);
        if (fsz > b.sz) {
            xo_assert(0);
            return -1;
        }
        if (f->tag == xo_log_tag_msg) {
            assert(f->type == xo_msg_type_len);
            assert(fsz == b.sz);
            h.packed = 0;
        } else {
            h.packed = 1;
        }
    } else {
        h.packed = 1;
    }

    xo_buf_move(&out, snprintf((char*)out.buf, out.sz, "%ld:%d:%s:%s:%d: ",
                               (long)h.ts, h.cpu, xo_log_level_name(e->level),
                               e->file, e->line));

    if (h.packed == 0) {
        xo_buf_move(&out, snprintf((char*)out.buf, out.sz,"%.*s",
                                   f->len, f->data));
    } else {
        xo_buf_move(&out, xo_fmt_printf((char*)out.buf, out.sz, e->fmt,
                                        e->args, e->nargs, b.buf, b.sz));
    }

    return out.buf - (uint8_t*)buf;
}
