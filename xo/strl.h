#pragma once

#include "xo.h"

size_t xo_strlcpy (char *dst, const char *src, size_t sz);

size_t xo_strlcat(char *dst, const char *src, size_t sz);
