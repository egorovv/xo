#pragma once

#include <string.h>
#include <stdint.h>
//#include <stdatomic.h>
//#include <stdalign.h>
#include <assert.h>
// offsetof
#include <stddef.h>

//printf
#include <stdio.h>

#include <sched.h>


#define xo_assert       assert

#define xo_cache_line 64

#define xo_field_size(st, m) sizeof(((st*)0)->m)

#if __ORDER_BIG_ENDIAN__

static inline uint64_t xo_tobe64(uint64_t x) {
   return __builtin_bswap64(x);
}
static inline uint64_t xo_tobe32(uint64_t x) {
   return __builtin_bswap32(x);
}
#elif __ORDER_LITTLE_ENDIAN__
static inline uint64_t xo_tobe64(uint64_t x) {
   return x
}
static inline uint64_t xo_tobe32(uint64_t x) {
   return x
}
#endif

#define xo_cas(v, o, n) __sync_bool_compare_and_swap(v, o, n)
#define xo_xchg(p, v) __sync_lock_test_and_set(p, v)

static inline int
xo_atomic_cmpset32(volatile uint32_t *var,
                       uint32_t *old, uint32_t new)
{
   return __sync_bool_compare_and_swap(var, *old, new);
}

static inline int
xo_atomic_cmpset64(volatile uint64_t *var,
                       uint64_t *old, uint64_t new)
{
   return __sync_bool_compare_and_swap(var, *old, new);
}

static inline int
xo_atomic_add32(volatile uint32_t *var, uint32_t val)
{
   return __sync_fetch_and_add(var, val);
}

static inline int
xo_atomic_add64(volatile uint64_t *var, uint64_t val)
{
   return __sync_fetch_and_add(var, val);
}


static inline void xo_pause() { __asm __volatile__("pause"); }
static inline void xo_yield() { sched_yield(); }
static inline void xo_wmb() { __sync_synchronize(); }
static inline void xo_rmb() { __sync_synchronize(); }

static inline void xo_cpuid(uint64_t op, uint64_t sub, uint64_t res[4])
{
    __asm__ ("  xchgq  %%rbx,%q1\n"
             "  cpuid\n"
             "  xchgq  %%rbx,%q1"
             : "=a"(res[0]), "=b" (res[1]), "=c"(res[2]), "=d"(res[3])
             : "0"(op), "2"(sub));
}

static inline uint64_t xo_rdtscp_cpu(uint32_t *cpu)
{
    uint32_t lo, hi;
   __asm__ volatile("rdtscp": "=a" (lo), "=d" (hi), "=c" (*cpu));
   return (uint64_t)lo | (uint64_t)hi << 32;
}

#ifdef __linux__
static inline int xo_getcpu() {
    uint32_t core = 0;
    xo_rdtscp_cpu(&core);
    return core & 0xfff; // | numaid << 12
}
#else
static inline int xo_getcpu() {
    uint64_t regs[4];
    xo_cpuid(1, 0, regs);
    return regs[1] >> 24;
}
#endif


#define xo_max(x, y)            \
   ({ typeof (x) _x = (x);      \
      typeof (y) _y = (y);      \
      _x > _y ? _x : _y; })

#define xo_min(x, y)            \
   ({ typeof (x) _x = (x);      \
      typeof (y) _y = (y);      \
      _x < _y ? _x : _y; })


#define xo_roundup(x, y)        \
   ({ typeof (x) _x = (x);      \
      typeof (y) _y = (y);      \
      ((_x - 1)/_y + 1) * y ; })


#define xo_id_merge2(a, b) a##b
#define xo_id_merge(a, b) xo_id_merge2(a, b)
#define xo_unique_id(x) xo_id_merge(x, __COUNTER__)

#define xo_cassert(x)                                           \
    enum { xo_unique_id(XO_ASSERT_) =  sizeof(char[(x)?1:-1]) }


#define xo_is_array(a)                          \
   __builtin_types_compatible_p(                \
      typeof(a), typeof(&a[0]))

#define xo_countof(a)                           \
   (sizeof(a)/sizeof(*(a)) + xo_is_array(a))


#define xo_likely(x)       __builtin_expect((uintptr_t)(x),1)
#define xo_unlikely(x)     __builtin_expect((uintptr_t)(x),0)

#ifdef __clang__
#define XO_SECTION(s) __attribute__ ((used,section("__DATA,"#s)))

#define XO_SECTION_STARTEND(type, s, start, end)                \
    extern type __start_##s __asm("section$start$__DATA$"#s);   \
    extern type __stop_##s __asm("section$end$__DATA$"#s);      \
    type *start = &__start_##s;                                 \
    type *end = &__stop_##s;
#else

#define XO_CONCAT(x,y) #x #y
#define XO_SECTION(s) __attribute__((used, section(XO_CONCAT(__, s))))

#define XO_SECTION_STARTEND(type, s, start, end)                  \
    extern type __start___##s;                                    \
    extern type __stop___##s;                                     \
    type *start = &__start___##s;                                 \
    type *end = &__stop___##s
#endif

