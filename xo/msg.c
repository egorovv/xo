
#include "msg.h"

static size_t xo_msg_int_size(uint64_t val);
static size_t xo_msg_tag_size(uint32_t tag);

static size_t xo_msg_pack_varint(uint8_t *buf, size_t sz, uint64_t val);
static size_t xo_msg_pack_tag(uint8_t *buf, size_t sz, uint8_t type, uint32_t tag);

static const size_t nopos = -1;

static inline size_t
xo_msg_parse_int(const uint8_t *buf, size_t sz, uint64_t *val)
{
    uint64_t x = 0;
    unsigned shift = 0;
    size_t   pos = 0;

    for (pos = 0; pos < sz; shift += 7, pos++) {
        x += (buf[pos] & 0x7f) << shift;
        if ((buf[pos] & 0x80) == 0) {
            *val = x;
            return pos + 1;
        }
    }
    return sz + 1;
}

static size_t xo_msg_parse_tag(const uint8_t *buf, size_t sz, struct xo_msg_field* f)
{
    uint64_t val = 0;
    size_t pos;

    pos = xo_msg_parse_int(buf, sz, &val);

    f->type = val & 7;
    f->tag = val >> 3;
    return pos;
}

size_t xo_msg_parse(uint8_t *buf, size_t sz, struct xo_msg_field* f)
{
    size_t pos = xo_msg_parse_tag(buf, sz, f);
    uint64_t val = 0;

    if (pos >= sz) {
        return sz + 1;
    }

    f->data = buf + pos;

    switch(f->type) {
    case xo_msg_type_varint:
        f->len = xo_msg_parse_int(f->data, sz, &f->val);
        break;
    case xo_msg_type_64bit:
        f->len = 8;
        memcpy(&f->val, f->data, f->len);
        f->val = xo_tobe64(f->val);
        break;
    case xo_msg_type_len:
        pos += xo_msg_parse_int(f->data, sz - pos, &val);
        f->len = val;
        f->data = buf + pos;
        break;
    case xo_msg_type_32bit: {
        int32_t x;
        f->len = 4;
        memcpy(&x, f->data, f->len);
        f->val = xo_tobe32(x);
        break;
    }
    default:
        return sz;
    }
    pos += f->len;
    return pos;
}


static void xo_msg_insert(struct xo_msg_field* msg, size_t len,
                          struct xo_msg_field* f)
{
    int i = len;
    for (; i > 0; --i) {
        if (msg[i - 1].tag > f->tag) {
            msg[i] = msg[i - 1];
        } else {
            break;
        }
    }
    msg[i] = *f;
}

int xo_msg_parse_all(uint8_t *buf, size_t sz, struct xo_msg_field* msg, size_t len)
{
    struct xo_msg_field fld;
    size_t pos = 0;
    size_t i = 0;
    for (;pos < sz; ++i) {
        pos += xo_msg_parse(buf + pos, sz - pos, &fld);
        if (pos > sz) {
            return -1;
        }
        if (i < len) {
            xo_msg_insert(msg, i, &fld);
        }
    }
    return i;
}


struct xo_msg_field*
xo_msg_find_tag(struct xo_msg_field* msg, size_t n, uint32_t tag)
{
    struct xo_msg_field *f = 0;
    size_t start = 0, end = n;
    for(;start < end;) {
        int mid = (start + end)/2;
        f = msg + mid;
        if (f->tag >= tag) {
            end = mid;
        } else {
            start = mid + 1;
        }
    }
    if (start < n  && msg[start].tag == tag) {
        return msg + start;
    } else {
        return 0;
    }
}

static size_t
xo_msg_int_size(uint64_t val)
{
    size_t sz = 1;
    while (val > 127){
        val >>= 7;
        ++sz;
    }
    return sz;
}


static size_t
xo_msg_pack_varint(uint8_t *buf, size_t sz, uint64_t val)
{
    size_t i = 0;
    do {
        if (i >= sz) {
            ;
        } else if (val > 127) {
            buf[i] = 0x80 | (val & 0x7f);
        } else {
            buf[i] = (val & 0x7f);
        }
        ++i;
        val >>= 7;
    } while (val > 0);

    return i;
}

static size_t xo_msg_tag_size(uint32_t tag)
{
    if (tag < 16) {
        return 1;
    } else {
        return 1 + xo_msg_int_size(tag >> 4);
    }
}

static size_t
xo_msg_left(size_t sz, size_t sub) {
    return sz > sub ? sz - sub : 0;
}

static size_t xo_msg_pack_tag(uint8_t *buf, size_t sz, uint8_t type, uint32_t tag)
{
    if (tag < 16) {
        if (sz) {
            buf[0] = type | ((tag & 0xf) << 3);
        }
        return 1;
    } else {
        if (sz) {
            buf[0] = type | ((tag & 0xf) << 3);
        }
        return 1 + xo_msg_pack_varint(buf + 1, xo_msg_left(sz, 1), tag >> 4);
    }
}

size_t xo_msg_pack(uint8_t *buf, size_t sz, struct xo_msg_field* f)
{
    size_t n = xo_msg_pack_tag(buf, sz, f->type, f->tag);

    switch(f->type) {
    case xo_msg_type_varint:
        n += xo_msg_pack_varint(buf + 1, xo_msg_left(sz, 1), f->val);
        break;
    case xo_msg_type_64bit:
        if (xo_msg_left(sz, n + 8)) {
            uint64_t x = xo_tobe64(f->val);
            memcpy(buf + n, &x, 8);
        }
        n += 8;
        break;
    case xo_msg_type_32bit:
        if (xo_msg_left(sz, n + 4)) {
            uint32_t x = xo_tobe32(f->val);
            memcpy(buf + n, &x, 4);
        }
        n += 4;
        break;
    case xo_msg_type_len:
        n += xo_msg_pack_varint(buf + n, xo_msg_left(sz, n), f->len);
        if (xo_msg_left(sz, n + f->len )) {
            memcpy(buf + n, f->data, f->len);
        }
        n += f->len;
        break;
    default:
        return 0;
    }
    return n;
}

size_t xo_msg_size(struct xo_msg_field* f)
{
    size_t n = xo_msg_tag_size(f->tag);

    switch(f->type) {
    case xo_msg_type_varint:
        n += xo_msg_int_size(f->val);
        break;
    case xo_msg_type_64bit:
        n += 8;
        break;
    case xo_msg_type_len:
        n += xo_msg_int_size(f->len);
        n += f->len;
        break;
    case xo_msg_type_32bit:
        n += 4;
        break;
    }
    return n;
}

size_t xo_msg_size_all(struct xo_msg_field* f, size_t len)
{
    size_t n = 0;
    size_t i = 0;

    for (; i < len; ++i) {
        n += xo_msg_size(f + i);
    }

    return n;
}

size_t xo_msg_pack_all(uint8_t *buf, size_t sz, struct xo_msg_field* f, size_t len)
{
    size_t pos = 0;
    size_t i = 0;

    for (; i < len; ++i) {
        xo_assert(len < sz);
        pos += xo_msg_pack(buf + pos , sz - pos, f + i);
    }

    return pos;
}
