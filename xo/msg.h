#pragma once

#include "xo.h"


typedef enum {
    xo_msg_type_varint = 0,
    xo_msg_type_64bit = 1,
    xo_msg_type_len = 2,
    //start group
    //end group
    xo_msg_type_32bit = 5,
} xo_msg_type;


struct xo_msg_field {
    uint32_t type; //xo_msg_type
    uint32_t tag;  // field tag
    uint64_t val;  // int data
    uint32_t len;  // input len
    uint8_t *data; // raw data
};

size_t xo_msg_parse(uint8_t *buf, size_t sz, struct xo_msg_field* f);
int    xo_msg_parse_all(uint8_t *buf, size_t sz, struct xo_msg_field* msg, size_t n);
struct xo_msg_field* xo_msg_find_tag(struct xo_msg_field* msg, size_t n, uint32_t tag);


size_t xo_msg_size(struct xo_msg_field* f);
size_t xo_msg_size_all(struct xo_msg_field* f, size_t len);

size_t xo_msg_pack(uint8_t *buf, size_t sz, struct xo_msg_field* f);
size_t xo_msg_pack_all(uint8_t *buf, size_t sz, struct xo_msg_field* f, size_t n);


static inline uint64_t xo_msg_zigzag(int64_t v) {
    if (v < 0) {
        return ~((uint64_t)v << 1);
    }  else {
        return (uint64_t)v << 1;
    }
}

static inline int64_t xo_msg_unzigzag(uint64_t v) {
    if (v & 1) {
        return (int64_t)~(v >> 1);
    }  else {
        return (int64_t)(v >> 1);
    }
}

static inline int64_t xo_msg_get_int(struct xo_msg_field *f) {
    return xo_msg_unzigzag(f->val);
}

static inline uint64_t xo_msg_get_uint(struct xo_msg_field *f) {
    return f->val;
}

static inline int64_t xo_msg_get_fixed(struct xo_msg_field *f) {
    return (int64_t)f->val;
}

static inline uint64_t xo_msg_get_ufixed(struct xo_msg_field *f) {
    return f->val;
}

static inline int32_t xo_msg_get_fixed32(struct xo_msg_field *f) {
    return (int32_t)f->val;
}

static inline uint32_t xo_msg_get_ufixed32(struct xo_msg_field *f) {
    return f->val;
}

static inline size_t xo_msg_pack_int(uint8_t *buf, size_t sz, uint32_t tag, int64_t val) {
    struct xo_msg_field f = { xo_msg_type_varint, tag, xo_msg_zigzag(val)};
    return xo_msg_pack(buf, sz, &f);
}

static inline size_t xo_msg_pack_uint(uint8_t *buf, size_t sz, uint32_t tag, uint64_t val) {
    struct xo_msg_field f = { xo_msg_type_varint, tag, val};
    return xo_msg_pack(buf, sz, &f);
}

static inline size_t xo_msg_pack_bytes(uint8_t *buf, size_t sz, uint32_t tag, uint8_t *val, size_t len) {
    struct xo_msg_field f = { xo_msg_type_len, tag, 0, len, val};
    return xo_msg_pack(buf, sz, &f);
}

static inline size_t xo_msg_pack_ufixed(uint8_t *buf, size_t sz, uint32_t tag, uint64_t val) {
    struct xo_msg_field f = { xo_msg_type_64bit, tag, val};
    return xo_msg_pack(buf, sz, &f);
}

static inline size_t xo_msg_pack_fixed(uint8_t *buf, size_t sz, uint32_t tag, int64_t val) {
    struct xo_msg_field f = { xo_msg_type_64bit, tag, (uint64_t)val};
    return xo_msg_pack(buf, sz, &f);
}

static inline size_t xo_msg_pack_ufixed32(uint8_t *buf, size_t sz, uint32_t tag, uint32_t val) {
    struct xo_msg_field f = { xo_msg_type_32bit, tag, val};
    return xo_msg_pack(buf, sz, &f);
}

static inline size_t xo_msg_pack_fixed32(uint8_t *buf, size_t sz, uint32_t tag, int32_t val) {
    struct xo_msg_field f = { xo_msg_type_32bit, tag, (uint64_t)val};
    return xo_msg_pack(buf, sz, &f);
}

static inline void xo_msg_pack_uint_f(struct xo_msg_field *f, uint32_t tag, uint64_t val) {
    struct xo_msg_field x = { xo_msg_type_varint, tag, val};
    *f = x;
}
static inline void xo_msg_pack_int_f(struct xo_msg_field *f, uint32_t tag, uint64_t val) {
    struct xo_msg_field x = { xo_msg_type_varint, tag, xo_msg_zigzag(val) };
    *f = x;
}
static inline void xo_msg_pack_fixed_f(struct xo_msg_field *f, uint32_t tag, int64_t val) {
    struct xo_msg_field x = { xo_msg_type_varint, tag, (uint64_t)val};
    *f = x;
}
static inline void xo_msg_pack_bytes_f(struct xo_msg_field *f, uint32_t tag, uint8_t *val, size_t len) {
    struct xo_msg_field x = { xo_msg_type_len, tag, 0, len, val};
    *f = x;
}
