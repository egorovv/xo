
#include "fmt.h"
#include "msg.h"
#include "buf.h"

#include <stdarg.h>

enum xo_fmt_token {
    xo_fmt_token_error = 0,
    xo_fmt_token_none,
    xo_fmt_token_percent,
    xo_fmt_token_dollar,
    xo_fmt_token_star,
    xo_fmt_token_num,
    xo_fmt_token_flag,
    // conv
    xo_fmt_token_i,
    xo_fmt_token_u,
    xo_fmt_token_x,
    xo_fmt_token_e,
    xo_fmt_token_f,
    xo_fmt_token_c,
    xo_fmt_token_s,
    xo_fmt_token_C,
    xo_fmt_token_S,
    xo_fmt_token_p,
    xo_fmt_token_n,
    xo_fmt_token_m,
    //length
    xo_fmt_token_h,
    xo_fmt_token_hh,
    xo_fmt_token_l,
    xo_fmt_token_ll,
    xo_fmt_token_L,
    xo_fmt_token_j,
    xo_fmt_token_z,
    xo_fmt_token_t,
};

struct xo_fmt_parse_pos {
    const char *start;
    const char *end;
    struct xo_fmt_arg *args;
    size_t nargs;
    size_t arg;
};


static enum xo_fmt_arg_type
xo_fmt_type(enum xo_fmt_token len, enum xo_fmt_token conv){

    switch (conv) {
    case xo_fmt_token_i:
    case xo_fmt_token_u:
    case xo_fmt_token_x:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_int;
        case xo_fmt_token_h:
            return xo_fmt_type_short;
        case xo_fmt_token_hh:
            return xo_fmt_type_char;
        case xo_fmt_token_l:
            return xo_fmt_type_long;
        case xo_fmt_token_ll:
            return xo_fmt_type_llong;
        case xo_fmt_token_j:
            return xo_fmt_type_maxint;
        case xo_fmt_token_t:
            return xo_fmt_type_ptrdiff;
        case xo_fmt_token_z:
            return xo_fmt_type_size;
        case xo_fmt_token_L:
        default:
            return xo_fmt_type_error;
        }
    case xo_fmt_token_e:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_double;
        case xo_fmt_token_L:
            return xo_fmt_type_ldouble;
        default:
            return xo_fmt_type_error;
        }

    case xo_fmt_token_f:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_float;
        case xo_fmt_token_L:
            return xo_fmt_type_ldouble;
        default:
            return xo_fmt_type_error;
        }
    case xo_fmt_token_c:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_char;
        case xo_fmt_token_l:
            return xo_fmt_type_wchar;
        default:
            return xo_fmt_type_error;
        }
        break;
    case xo_fmt_token_s:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_str;
        case xo_fmt_token_l:
            return xo_fmt_type_wstr;
        default:
            return xo_fmt_type_error;
        }
        break;
    case xo_fmt_token_C:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_wchar;
        default:
            return xo_fmt_type_error;
        }
        break;
    case xo_fmt_token_S:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_str;
        default:
            return xo_fmt_type_error;
        }
        break;
    case xo_fmt_token_p:
        switch (len) {
        case xo_fmt_token_none:
            return xo_fmt_type_ptr;
        default:
            return xo_fmt_type_error;
        }
    case xo_fmt_token_n:
    case xo_fmt_token_m:
    default:
        return xo_fmt_type_error;
    }
}


static enum xo_fmt_token
xo_fmt_parse_text(struct xo_fmt_parse_pos *pos)
{
    for (; *pos->end; ++pos->end) {
        if (*pos->end == '%') {
            ++pos->end;
            if (*pos->end != '%') {
                return xo_fmt_token_percent;
            }
        }
    }
    return xo_fmt_token_none;
}


static enum xo_fmt_token
xo_fmt_parse_num(struct xo_fmt_parse_pos *pos, int *n)
{
    int i = 0;
    int val = 0;

    for (; *pos->end; ++pos->end) {
        switch (*pos->end) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            ++i;
            val = 10*val + *pos->end - '0';
            continue;
        default:
            break;
        }
        break;
    }
    *n = val;
    return i ? xo_fmt_token_num : xo_fmt_token_none;
}


static enum xo_fmt_token
xo_fmt_parse_width(struct xo_fmt_parse_pos *pos, int *n)
{
    if (*pos->end == '*') {
        ++pos->end;
        return xo_fmt_token_star;
    }

    return xo_fmt_parse_num(pos, n);
}

static enum xo_fmt_token
xo_fmt_parse_prec(struct xo_fmt_parse_pos *pos, int *n)
{
    if (*pos->end != '.') {
        return xo_fmt_token_none;
    }
    ++pos->end;
    return xo_fmt_parse_width(pos, n);
}


static enum xo_fmt_token
xo_fmt_parse_flag(struct xo_fmt_parse_pos *pos)
{
    for (;*pos->end;++pos->end) {
        switch (*pos->end) {
        case '#':
        case '0':
        case '-':
        case ' ':
        case '+':
        case '\'':
        case 'I':
            break;
        default:
            return xo_fmt_token_flag;
        }
    }
    return xo_fmt_token_error;
}

static enum xo_fmt_token
xo_fmt_parse_length(struct xo_fmt_parse_pos *pos)
{
    switch (*pos->end) {
    case 'h':
        ++pos->end;
        if (*pos->end == 'h')  {
            ++pos->end;
            return xo_fmt_token_hh;
        } else {
            return xo_fmt_token_h;
        }
        break;
    case 'l':
        ++pos->end;
        if (*pos->end == 'l')  {
            ++pos->end;
            return xo_fmt_token_ll;
        } else {
            return xo_fmt_token_l;
        }
        break;
    case 'L':
        ++pos->end;
        return xo_fmt_token_L;
        break;
    case 'j':
        ++pos->end;
        return xo_fmt_token_j;
        break;
    case 'z':
        ++pos->end;
        return xo_fmt_token_z;
        break;
    case 't':
        ++pos->end;
        return xo_fmt_token_t;
        break;
    }
    return xo_fmt_token_none;
}

static enum xo_fmt_token
xo_fmt_parse_conv(struct xo_fmt_parse_pos *pos)
{
    switch (*pos->end) {
    case 'd':
    case 'i':
        ++pos->end;
        return xo_fmt_token_i;
        break;
    case 'o':
    case 'u':
        ++pos->end;
        return xo_fmt_token_u;
        break;
    case 'x':
    case 'X':
        ++pos->end;
        return xo_fmt_token_x;
        break;
    case 'e':
    case 'E':
    case 'g':
    case 'G':
    case 'a':
    case 'A':
        ++pos->end;
        return xo_fmt_token_e;
        break;
    case 'f':
    case 'F':
        ++pos->end;
        return xo_fmt_token_f;
        break;
    case 'c':
        ++pos->end;
        return xo_fmt_token_c;
        break;
    case 's':
        ++pos->end;
        return xo_fmt_token_s;
        break;
    case 'C':
        ++pos->end;
        return xo_fmt_token_C;
        break;
    case 'S':
        ++pos->end;
        return xo_fmt_token_S;
        break;
    case 'p':
        ++pos->end;
        return xo_fmt_token_p;
        break;
    case 'n':
        ++pos->end;
        return xo_fmt_token_n;
        break;
    case 'm':
        ++pos->end;
        return xo_fmt_token_m;
        break;
    }
    return xo_fmt_token_error;
}


static enum xo_fmt_token
xo_fmt_parse_field(struct xo_fmt_parse_pos *pos)
{
    enum xo_fmt_token rc;
    enum xo_fmt_token l;
    struct xo_fmt_arg arg = {};
    int w, p;

    arg.pos = pos->end - pos->start - 1;

    rc = xo_fmt_parse_flag(pos);
    if (rc == xo_fmt_token_error) {
        return rc;
    }

    rc = xo_fmt_parse_width(pos, &w);
    if (rc == xo_fmt_token_error) {
        return rc;
    } else if (rc == xo_fmt_token_star) {
        arg.width = 1;
    }

    rc = xo_fmt_parse_prec(pos, &p);
    if (rc == xo_fmt_token_error) {
        return rc;
    } else if (rc == xo_fmt_token_star) {
        arg.prec = 1;
    } else if (rc == xo_fmt_token_num) {
        arg.sz = p;
    }

    l = rc = xo_fmt_parse_length(pos);
    if (rc == xo_fmt_token_error) {
        return rc;
    }

    rc = xo_fmt_parse_conv(pos);
    if (rc == xo_fmt_token_error) {
        return rc;
    }

    arg.type = xo_fmt_type(l, rc);
    arg.len = pos->end - pos->start - arg.pos;
/*
    if (arg.len >= sizeof(arg.fmt)) {
        return xo_fmt_token_error;
    }
    memcpy(arg.fmt, pos->start + arg.pos, arg.len);
*/

    if (arg.type == xo_fmt_type_error) {
        return xo_fmt_token_error;
    }
    if (pos->arg < pos->nargs) {
        pos->args[pos->arg] = arg;
    }
    ++pos->arg;

    return rc;
}

int
xo_fmt_parse(const char *fmt, struct xo_fmt_arg *args, size_t nargs)
{
    struct xo_fmt_parse_pos pos = {fmt, fmt, args, nargs};
    enum xo_fmt_token rc;
    for (;*pos.end;) {
        rc = xo_fmt_parse_text(&pos);
        if (rc != xo_fmt_token_percent) {
            return pos.arg;
        }
        rc = xo_fmt_parse_field(&pos);
        if (rc == xo_fmt_token_error) {
            return -1;
        }
    }
    return pos.arg;
}

static inline int xo_strnlen(const char *s, size_t sz) {
    size_t len = 0;
    while (len < sz && s[len] != 0) {
        ++len;
    }
    return len;
}
static inline int xo_strlen(const char *s) {
    size_t len = 0;
    while (s[len] != 0) {
        ++len;
    }
    return len;
}

int
xo_fmt_va_pack(uint8_t* buf, size_t sz, struct xo_fmt_arg *args, size_t nargs, va_list ap)
{
    int w = 0, p = 0;
    char *s;
    int n;
    size_t off = 0;
    size_t i = 0;

    struct xo_buf out = { sz, buf};

    for (; i < nargs; ++i) {
        if (args[i].width) {
            w = va_arg(ap, int);
            off = xo_msg_pack_uint(out.buf, out.sz, xo_fmt_tag_width, w);
            xo_buf_move(&out, off);
        }

        if (args[i].prec) {
            p = va_arg(ap, int);
            off = xo_msg_pack_uint(out.buf, out.sz, xo_fmt_tag_prec, p);
            xo_buf_move(&out, off);
        }

#define xo_fmt_pack_int(arg)                                         \
        off = xo_msg_pack_int(out.buf, out.sz, xo_fmt_tag_val, arg); \
        xo_buf_move(&out, off);
#define xo_fmt_pack_bytes(arg, argsz)                                   \
        off = xo_msg_pack_bytes(out.buf, out.sz, xo_fmt_tag_val, arg, argsz); \
        xo_buf_move(&out, off);

        switch (args[i].type) {
        case xo_fmt_type_str:
        case xo_fmt_type_wstr:
            s = va_arg(ap, char*);
            if (s == 0) {
                s = (char*)"(null)";
            }
            if (args[i].prec) {
                n = xo_strnlen(s, p);
            } else if (args[i].sz) {
                n = xo_strnlen(s, args[i].sz);
            } else {
                n = xo_strlen(s) + 1;
            }
            xo_fmt_pack_bytes((uint8_t*)s, n);
            break;
        case xo_fmt_type_char:
            xo_fmt_pack_int(va_arg(ap, unsigned int));
            break;
        case xo_fmt_type_wchar:
            xo_fmt_pack_int(va_arg(ap, wchar_t));
            break;
        case xo_fmt_type_short:
            xo_fmt_pack_int(va_arg(ap, int));
            break;
        case xo_fmt_type_int:
            xo_fmt_pack_int(va_arg(ap, int));
            break;
        case xo_fmt_type_long:
            xo_fmt_pack_int(va_arg(ap, long));
            break;
        case xo_fmt_type_llong:
        case xo_fmt_type_maxint:
            xo_fmt_pack_int(va_arg(ap, long long));
            break;
        case xo_fmt_type_ptrdiff:
            xo_fmt_pack_int(va_arg(ap, ptrdiff_t));
            break;
        case xo_fmt_type_size:
            xo_fmt_pack_int(va_arg(ap, size_t));
            break;
        case xo_fmt_type_ptr:
            off = xo_msg_pack_fixed(out.buf, out.sz, xo_fmt_tag_val,
                                    (uintptr_t)va_arg(ap, void*));
            xo_buf_move(&out, off);
            break;
        default:
            break;
        }
    }
    return out.buf - buf;

#undef xo_fmt_pack_int
#undef xo_fmt_pack_bytes

}

int
xo_fmt_vamsg(struct xo_msg_field* msg, size_t sz, struct xo_fmt_arg *args, size_t nargs, va_list ap)
{
    int w = 0, p = 0;
    char *s;
    int n;
    size_t i = 0;
    struct xo_msg_field* start = msg;

    for (; i < nargs; ++i) {
        if (args[i].width) {
            w = va_arg(ap, int);
            xo_msg_pack_uint_f(msg++, xo_fmt_tag_width, w);
        }

        if (args[i].prec) {
            p = va_arg(ap, int);
            xo_msg_pack_uint_f(msg++, xo_fmt_tag_prec, p);
        }

#define xo_fmt_pack_int(arg)                            \
        xo_msg_pack_int_f(msg++, xo_fmt_tag_val, arg);

#define xo_fmt_pack_bytes(arg, argsz)                         \
        xo_msg_pack_bytes_f(msg++, xo_fmt_tag_val, arg, argsz);

        switch (args[i].type) {
        case xo_fmt_type_str:
        case xo_fmt_type_wstr:
            s = va_arg(ap, char*);
            if (args[i].prec) {
                n = xo_strnlen(s, p);
            } else if (args[i].sz) {
                n = xo_strnlen(s, args[i].sz);
            } else {
                n = xo_strlen(s) + 1;
            }
            xo_fmt_pack_bytes((uint8_t*)s, n);
            break;
        case xo_fmt_type_char:
            xo_fmt_pack_int(va_arg(ap, unsigned int));
            break;
        case xo_fmt_type_wchar:
            xo_fmt_pack_int(va_arg(ap, wchar_t));
            break;
        case xo_fmt_type_short:
            xo_fmt_pack_int(va_arg(ap, int));
            break;
        case xo_fmt_type_int:
            xo_fmt_pack_int(va_arg(ap, int));
            break;
        case xo_fmt_type_long:
            xo_fmt_pack_int(va_arg(ap, long));
            break;
        case xo_fmt_type_llong:
        case xo_fmt_type_maxint:
            xo_fmt_pack_int(va_arg(ap, long long));
            break;
        case xo_fmt_type_ptrdiff:
            xo_fmt_pack_int(va_arg(ap, ptrdiff_t));
            break;
        case xo_fmt_type_size:
            xo_fmt_pack_int(va_arg(ap, size_t));
            break;
        case xo_fmt_type_ptr:
            xo_msg_pack_fixed_f(msg++, xo_fmt_tag_val,
                                (uintptr_t)va_arg(ap, void*));
            break;
        default:
            break;
        }
    }
    return msg - start;

#undef xo_fmt_pack_int
#undef xo_fmt_pack_bytes

}


int xo_fmt_pack(uint8_t* buf, size_t sz, const char *fmt, struct xo_fmt_arg *args, size_t nargs, ...)
{
    int res;
    va_list ap;
    va_start(ap, nargs);
    res = xo_fmt_va_pack(buf, sz, args, nargs, ap);
    va_end(ap);
    return res;
}

void *xo_fmt_msg_get_ptr(struct xo_msg_field *f) {
    return (void*)f->val;
}

int xo_fmt_printf(char* buf, size_t sz, const char *fmt,
                  struct xo_fmt_arg *args, size_t nargs,
                  uint8_t *msg, size_t msz)
{
    size_t pos = 0;
    struct xo_msg_field f;

    size_t fpos = 0;

    int w = 0, p = 0;

    struct xo_buf b = { sz, (uint8_t*)buf };
    size_t out = 0;
    size_t i;
    char ffmt[xo_fmt_max_len + 1] = {};

    for (i = 0 ; i < nargs; ++i) {
        if (args[i].width) {
            pos += xo_msg_parse(msg + pos, msz - pos, &f);
            if (pos > sz || f.tag != xo_fmt_tag_width || f.type != xo_msg_type_varint) {
                return -1;
            }
            w = f.val;
        }

        if (args[i].prec) {
            pos += xo_msg_parse(msg + pos, msz - pos, &f);
            if (pos > sz || f.tag != xo_fmt_tag_prec || f.type != xo_msg_type_varint) {
                return -1;
            }
            p = f.val;
        }

        pos += xo_msg_parse(msg + pos, msz - pos, &f);
        if (pos > sz || f.tag != xo_fmt_tag_val) {
            return -1;
        }

        // copy text
        out += xo_buf_write(&b, (uint8_t*)fmt + fpos, args[i].pos - fpos);
        memcpy(ffmt, (uint8_t*)fmt + args[i].pos, args[i].len);
        ffmt[args[i].len] = 0;

#define xo_fmt_print_intf(type, fn)                                     \
        do {                                                            \
            type x = (fn)(&f);                                          \
            size_t n = 0;                                               \
            if (args[i].width && args[i].prec) {                        \
                n = snprintf((char*)b.buf, b.sz, ffmt, w, p, x);        \
            } else if (args[i].width) {                                 \
                n = snprintf((char*)b.buf, b.sz, ffmt, w, x);           \
            } else if (args[i].prec) {                                  \
                n = snprintf((char*)b.buf, b.sz, ffmt, p, x);           \
            } else {                                                    \
                n = snprintf((char*)b.buf, b.sz, ffmt, x);              \
            }                                                           \
            out += n;                                                   \
            xo_buf_move(&b, n);                                         \
        } while(0)

#define xo_fmt_print_int(type) xo_fmt_print_intf(type, xo_msg_get_int)

#define xo_fmt_print_ptr() xo_fmt_print_intf(void *, xo_fmt_msg_get_ptr)

#define xo_fmt_print_str()                                              \
        do {                                                            \
            size_t n = 0;                                               \
            if (args[i].width && args[i].prec) {                        \
                n = snprintf((char*)b.buf, b.sz, ffmt, w, f.len, f.data); \
            } else if (args[i].width) {                                 \
                n = snprintf((char*)b.buf, b.sz, ffmt, w, f.data);      \
            } else if (args[i].prec) {                                  \
                n = snprintf((char*)b.buf, b.sz, ffmt, f.len, f.data);  \
            } else {                                                    \
                n = snprintf((char*)b.buf, b.sz, ffmt, f.data);         \
            }                                                           \
            out += n;                                                   \
            xo_buf_move(&b, n);                                         \
        } while(0)

        switch (args[i].type) {
        case xo_fmt_type_char:
        case xo_fmt_type_short:
        case xo_fmt_type_int:
            xo_fmt_print_int(int);
            break;
        case xo_fmt_type_wchar:
            xo_fmt_print_int(wchar_t);
            break;
        case xo_fmt_type_long:
            xo_fmt_print_int(long);
            break;
        case xo_fmt_type_llong:
        case xo_fmt_type_maxint:
            xo_fmt_print_int(long long);
            break;
        case xo_fmt_type_ptrdiff:
            xo_fmt_print_int(ptrdiff_t);
            break;
        case xo_fmt_type_size:
            xo_fmt_print_int(size_t);
            break;
        case xo_fmt_type_str:
        case xo_fmt_type_wstr:
            xo_fmt_print_str();
            break;
        case xo_fmt_type_ptr:
            xo_fmt_print_ptr();
            break;
        default:
            break;
        }
        fpos = args[i].pos + args[i].len;
    }

    // copy format
    out += xo_buf_write(&b, (uint8_t*)fmt + fpos, xo_strlen(fmt + fpos) + 1);

    return out - 1; //except last 0
}

