#pragma once

#include "xo.h"

struct xo_test_def {
   const char *name;
   int (*fn)();
};

#define XO_TEST(f) \
   static int xo_test_##f();                                     \
   static struct xo_test_def XO_SECTION(xo_test)                 \
   __attribute__((used))                                         \
   f = { #f, xo_test_##f};                                       \
   static int xo_test_##f()


extern struct xo_test_def *xo_test_def_first, *xo_test_def_last;


int xo_test_run(int argc, char **argv);
