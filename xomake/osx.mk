# commands
CC = cc
CXX = c++
AR = ar
LD = cc
AS = as

CP = cp
MKDIR = mkdir -p
RM = rm -f 
RMDIR = rm -fr

CCOMPILE   = $(CC) -c -MD -MP -MT $@ -MF $@.d $(CFLAGS) $(CPPFLAGS) -o $@ $<
CXXCOMPILE = $(CXX) -c -MD -MP -MT $@ -MF $@.d $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<
MKLIB      = rm -f $@ && $(AR) cr $@ $(OBJS)
MKPROG     = $(LD) -o $@ $(OBJS) $(DEPLIBS) $(DEPDLLLIBS) $(LDFLAGS) 
MKDLL      = $(LD) -o $@ -shared  $(OBJS) $(DEPDLLLIBS) $(LDFLAGS)
INSTALL    = install 
MKINSTALL    = ln -s $(abspath $<) $@


#names
O_EXT = .o
LIB_EXT = .a
PROG_EXT = 
DLL_EXT = .so

xlibname  = $(OUT_ROOT)/lib/$(1)$(LIB_EXT)
xprogname = $(OUT_ROOT)/bin/$(1)$(PROG_EXT)
xdllname  = $(OUT_ROOT)/lib/lib$(1)$(DLL_EXT)
xdlllib   = $(OUT_ROOT)/lib/lib$(1)$(DLL_EXT)
xsoname   = $(1:$(OUT_ROOT)/lib/%=%)


#flags
CPPFLAGS += -I$(OUT_ROOT)/include

COPTFLAGS = -O0

CFLAGS += -g -fPIC \
	-Wall \
	-Werror \
	$(COPTFLAGS)

CXXFLAGS += -std=gnu++0x


LDFLAGS += -L$(OUT_ROOT)/lib
LDFLAGS += -g
LDFLAGS += -lstdc++


