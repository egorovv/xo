# -*- mode: makefile -*-
ifeq ($(realpath .),)
$(error make 3.81 required)
endif

MAKEFLAGS += --no-builtin-rules --output-sync=target --print-directory

#disable buildin rules
.SUFFIXES:


ifdef XO_TRACE
trace = $(warning $0($1,$2,$3,$4,$5,$6,$7,$8,$9))
endif

ifdef XO_DEBUG
debug = $(warning $(value 1))
endif

XO_FILE ?= $(notdir $(firstword $(MAKEFILE_LIST)))
XO_PATH ?= $(abspath $(lastword $(MAKEFILE_LIST)))
XO_ROOT ?= $(dir $(XO_PATH))
XO_ROOT := $(abspath $(XO_ROOT))
XO_LIB  := $(XO_ROOT)

export XO_PATH
export XO_ROOT
export XO_FILE

SRC_ROOT ?= $(dir $(XO_ROOT))

ifneq ($(Q),)
override Q := @
endif

ifneq ($(Q),)

define run
	@echo $1
	@$2
endef

else

define run
	$2
endef

endif

# path relative to the the root 
relpath = $(trace)$(patsubst /%,%,$(patsubst $(realpath $(SRC_ROOT))%,%,$(realpath $1)))
uniq = $(if $1,$(call uniq,$(filter-out $(lastword $1),$1)) $(lastword $1),)
# resolve leading ./ and ../
normmod = $(if $(filter ./% ../%, $1),$(call relpath,$(MODULE)/$1),$1)
adduniq = $1 += $(filter-out $($1),$2)

xo-car = $(firstword $1)
xo-cdr = $(wordlist 2,$(words $1),$1)


.PHONY: all clean reallyclean test

#################################################################
# first pass
ifndef MODULE

MODULE := $(call relpath,.)
ifeq ($(MODULE),)
MODULE := .
endif

# don't do clean all in parallel
.NOTPARALLEL:

$(call uniq,all test clean reallyclean $(MAKECMDGOALS)):
	$(Q)$(MAKE) -C $(SRC_ROOT) -f $(XO_PATH) MODULE=$(MODULE) $@

###############################################################
#sub-make
else # MODULE defined

.SECONDEXPANSION:

ifndef _XO_
_XO_ := 1

ifndef XO_OS
	XO_OS := $(shell uname)
	ifeq ($(XO_OS),Linux)
		XO_OS := linux
	else ifeq ($(XO_OS),Darwin)
		XO_OS := osx
	else
		XO_OS := default
	endif
endif


override SRC_ROOT := .
SRC_ABSROOT := $(realpath .)

OUT_ROOT = out/$(XO_OS)
INT_ROOT = $(OUT_ROOT)/obj

####################################################################################

include $(XO_LIB)/$(XO_OS).mk
#-include $(wildcard $(XO_ROOT)/xo-*.mk)

# all build system files do far
XO_LIST := $(MAKEFILE_LIST)

.PRECIOUS: %/.

%/.:
	$(Q)$(MKDIR) $*

#define main targets
.PHONY: all-dirs all-clean all-tests

all: all-dirs

all-dirs: | $(OUT_ROOT)/lib/. $(OUT_ROOT)/bin/. 

clean: $(MODULE)-clean

all-clean: 

reallyclean: all-clean
	$(Q)$(RMDIR) $(OUT_ROOT)
	$(Q)$(RMDIR) $(INT_ROOT)

test: $(MODULE)-tests

all-tests:


####################################################################################
# 1 - module
# 2 - install group ( $(mod)-$(targ)-$(group)
# 3 - target files
define make-install-int
$(trace)
$1-install: $2-install
$2-install: $3

$3: $(OUT_ROOT)/$($2_DIR)/% : $1/% | $(OUT_ROOT)/$($2_DIR)/. $$$$(@D)/.
	$(call run,$$@,$$(MKINSTALL))

$1_OUT += $3
endef

####################################################################################
# $1 - module
# $2 - install group ( $(mod)-$(targ)-$(group)
define make-install
$(trace)
$(if $($2_FILES),
$(call make-install-int,$1,$2,$(addprefix $(OUT_ROOT)/$($2_DIR)/,$($2_FILES)))
)
endef
####################################################################################

####################################################################################
# $1 - module
# $2 - lib
define make-hdrs
$(trace)$(eval $1_INSTALL += $1-$2-headers
$1-$2-headers_DIR = include/$2
$1-$2-headers_FILES = $($1_$2_HDRS))
endef


####################################################################################
# $1 - module
# $2 - src
# $3 - dst
define make-install-file-int
$(trace)
$1-build: $(OUT_ROOT)/$3

$(OUT_ROOT)/$3: $1/$2 | $$$$(@D)/.
	$(RM) $(OUT_ROOT)/$3
	$(LN) $(SRC_ABSROOT)/$1/$2 $(OUT_ROOT)/$3
$1_OUT += $(OUT_ROOT)/$3
endef

make-install-src = $(call xo-car,$(subst :, ,$1))
make-install-dst = $(call xo-cdr,$(subst :, ,$1))

define make-install-file
$(trace)
$(if $(call make-install-dst,$2),
$(call make-install-file-int,$1,$(call make-install-src,$2),$(call make-install-dst,$2)),
$(call make-install-file-int,$1,$2,$2))
endef



####################################################################################
XO_TARGET_HOOKS += xo-make-c-objs

# mod,targ,flav,ext
define xo-make-c-obj-names
$(trace)$(addsuffix $(O_EXT),$(addprefix $(INT_DIR)/$2/,$(filter $4,$($1_$2_SRCS))))
endef

# mod,targ,flav,bin
define xo-make-c-objs
$(call make-obj,$1,$2,$3,$4,$1,$(call xo-make-c-obj-names,$1,$2,$3,%.c),$$(CCOMPILE))
$(call make-obj,$1,$2,$3,$4,$1,$(call xo-make-c-obj-names,$1,$2,$3,%.cc %.cpp %.cxx),$$(CXXCOMPILE))
endef

####################################################################################
# 1 - mod
# 2 - targ 
# 3 - flav
# 4 - bin
# 5 - srcdir
# 6 - obj
# 7 - cmd
define make-obj
$(trace)$(if $6, $(if $(XO_DIRTY),
$6: $$$$($1_$2_DEPS) $$$$($1_DEPENDS) | $$$$(@D)/.,
$6: $$(XO_LIST) $1/$(XO_FILE) $$$$($1_$2_DEPS) $$$$($1_DEPENDS)| $$$$(@D)/.
)
$6: CFLAGS = $(CFLAGS) $$($1_CFLAGS) $$($1_$2_CFLAGS)
$6: $(INT_DIR)/$2/%$(O_EXT) : $5/% 
	$(call run,$$<,$7)
-include $(6:%=%.d)
$1_$2_OBJS += $6
)
endef

####################################################################################
# 1 - mod
# 2 - targ 
# 3 - targ file
# 4 - link command
# 5 - flav
define make-bin-int
$(trace)
####### $1-$2 ==> $3 #######
.PHONY: $1-$2-build $1-$2-pre-build $1-$2-clean

$1-$5: $1-$2-build

$1-clean: $1-$2-clean

$1-$2-build: $3

$1-$2-pre-build:

$1-$2-clean: 
	$(Q)$(RM) $3

$3: $$$$($1_$2_OBJS) $$$$($1_DEPLIBS) $$$$($1_DEPDLLLIBS) | $$$$(@D)/.
	$(call run,$$@,$4)

$3: OBJS = $$($1_$2_OBJS)
$3: DEPLIBS = $$($1_$2_DEPLIBS) $$($1_DEPLIBS) 
$3: DEPDLLLIBS = $$($1_$2_DEPDLLLIBS) $$($1_DEPDLLLIBS)
$3: LDFLAGS = $(LDFLAGS) $$($1_LDFLAGS) $$($1_$2_LDFLAGS)

$(foreach hook,$(XO_TARGET_HOOKS),
####### $(hook),$1,$2,$5,$3 ####
$(call $(hook),$1,$2,$5,$3))
####### end $1-$2 ==> $3 #######
endef
####################################################################################


####################################################################################
# 1 - mod
# 2 - targ 
# 3 - targ file
# 4 - rule
# 5 - prog/lib/dll
define make-bin
$(trace)$(call make-bin-int,$1,$2,$3,$4,$5,$($1_$2_SRCS))
endef
####################################################################################

####################################################################################
# $1 - module
# $2 - lib
define make-lib
$(trace)$(call make-bin,$1,$2,$(call xlibname,$2),$(value MKLIB),libs)
$(call make-hdrs,$1,$2)
$1_EXPLIBS += $(call xlibname,$2)
endef
####################################################################################

####################################################################################
# $1 - module
# $2 - lib
define make-prog
$(trace)
$(call make-bin,$1,$2,$(call xprogname,$2),$(value MKPROG),progs)
$1_$2_DEPLIBS += $$($1_EXPLIBS)
$1_$2_DEPDLLLIBS += $$($1_EXPDLLLIBS)
$1_$2_DEPS += $$($1_EXPLIBS) $$($1_EXPDLLLIBS)
endef
####################################################################################

####################################################################################
# $1 - module
# $2 - dll
define make-dll
$(trace)
$(call make-bin,$1,$2,$(call xdllname,$2),$(value MKDLL),dlls)
$(call make-hdrs,$1,$2)
$(call make-dll-lib-rule,$1,$2)
$1_$2_DEPLIBS = $$($1_EXPLIBS)
$1_EXPDLLLIBS += $(call xdlllib,$2)
$1_$2_DEPS += $$($1_EXPLIBS)
endef

####################################################################################
define make-test-int
$(trace)
$(call make-bin-int,$1,test,$2,$(value MKPROG),tests,$($1_TESTS))

$1-tests: $2
	$2

$1_test_DEPLIBS = $$($1_EXPLIBS)
$1_test_DEPDLLLIBS = $$($1_EXPDLLLIBS)
$1_test_DEPS += $$($1_EXPLIBS) $$($1_EXPDLLLIBS) $$($1_OUT)
endef

####################################################################################
define make-test
$(trace)
$(if $($1_TESTS),
$(eval $1_test_SRCS := $($1_TESTS))
$(call make-test-int,$1,$(OUT_ROOT)/test/$1-test$(PROG_EXT))
)
endef

####################################################################################
define mktargets
$(addprefix $1-,deps dirs install libs dlls progs)
endef

####################################################################################
define module-int
$(trace)
####### module $(MODULE) #######
.PHONY: $(MODULE)-build $(MODULE)-clean $(MODULE)-tests $(call mktargets,$(MODULE))

all:  $(MODULE)-build

all-tests: $(MODULE)-tests

$(MODULE)-build: $(call mktargets,$(MODULE))

$(MODULE)-deps:

$(MODULE)-libs:

$(MODULE)-dlls: $(MODULE)-libs

$(MODULE)-progs: $(MODULE)-dlls

$(MODULE)-dirs: | $(INT_ROOT)/$(MODULE)/.

all-clean:  $(MODULE)-clean

$(MODULE)-tests: $(MODULE)-build

$(MODULE)-clean: $(MODULE)-clean-int

$(MODULE)-clean-int:
	$(Q)$(RMDIR) $(INT_ROOT)/$(MODULE:%.=%)/* $$($(MODULE)_OUT)

####### module $(MODULE) targets #######
$(foreach lib,$($(MODULE)_LIBS),$(call make-lib,$(MODULE),$(lib)))
$(foreach dll,$($(MODULE)_DLLS),$(call make-dll,$(MODULE),$(dll)))
$(foreach prog, $($(MODULE)_PROGS),$(call make-prog,$(MODULE),$(prog)))
$(foreach inst,$($(MODULE)_INSTALL),$(call make-install,$(MODULE),$(inst)))
$(foreach file,$($(MODULE)_FILES) $($(MODULE)_DIRS),$(call make-install-file,$(MODULE),$(file)))

$(call make-test,$(MODULE))

$(foreach hook,$(XO_HOOKS),$(call $(hook),$(MODULE)))

####### end module $(MODULE) #######

$(foreach dep,$($(MODULE)_DEPS),$(call make-depends,$(call normmod,$(dep))))

endef
####################################################################################


####################################################################################
define module-debug
$(debug)$(eval $1)
endef

####################################################################################
# 1 - proj
define module
$(call module-debug,$(call module-int))
endef

####################################################################################
define module-include-int
$(trace)

override MODULE := $1
override INT_DIR := $(INT_ROOT)/$1
override SRC_DIR := $1
override srcdir := $1
override builddir := $(INT_ROOT)/$1

include $1/$(XO_FILE)

$$(foreach hook,$(XO_PRE_HOOKS),$$(call $$(hook),$1))

$$(call module)

$1_included := 1
override MODULE := $(MODULE)
override INT_DIR := $(INT_ROOT)/$(MODULE)
override SRC_DIR := $(MODULE)

endef

####################################################################################
define module-include
$(if $($1_included),,$(call module-debug,$(call module-include-int,$1)))
endef

####################################################################################
define module-depend-int
$(MODULE)-deps: $1-build

$(if $($1_EXPDLLLIBS),
$(call adduniq,$(MODULE)_DEPLIBS,$($1_EXPLIBS)),
$(call adduniq,$(MODULE)_DEPLIBS,$($1_DEPLIBS) $($1_EXPLIBS))
)
$(call adduniq,$(MODULE)_DEPDLLLIBS,$($1_EXPDLLLIBS))

$(foreach hook,$(XO_IMPORT_HOOKS),$(call $(hook),$1))
endef

define module-depend
$(call module-debug,$(call module-depend-int,$1))
endef

####################################################################################
# 1 - module
#
define make-depends-int
$(trace)
$(call module-include,$1)
$(call module-depend,$1)
endef

####################################################################################
define make-depends
$(trace)
$(if $(realpath $1/$(XO_FILE)),$(call make-depends-int,$1),$(error Module $1 not found))
endef

######## run it ##########

$(call module-include,$(MODULE))

endif #_XO_
endif #MODULE
