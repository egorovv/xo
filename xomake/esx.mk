
DEV_BUILDTYPE ?= beta
TARGET_ESX_VERSION ?= 6.0
ESX_TARGET    ?= $(TARGET_ESX_VERSION)


ESXDP_ROOT ?= $(XO_ROOT)

BUILD_DIR     = $(ESXDP_ROOT)/build
DEVKITROOT    = ${BUILD_DIR}/devkit
DEVKIT_ENV-$(TARGET_ESX_VERSION) := ${DEVKITROOT}/devkit_$(ESX_TARGET).env
DEVKIT_KMDK_INC-$(TARGET_ESX_VERSION) := ${DEVKITROOT}/kmdk_$(ESX_TARGET).inc

TARGET_SUBST := $(subst .,,$(TARGET_ESX_VERSION))
DEVKIT_ENV_$(TARGET_SUBST) := ${DEVKITROOT}/devkit_$(TARGET_ESX_VERSION).env
DEVKIT_KMDK_INC_$(TARGET_SUBST) := ${DEVKITROOT}/kmdk_$(TARGET_ESX_VERSION).inc

REPOROOT := $(abspath $(ESXDP_ROOT)/../..)

_MODULE := $(MODULE)
override MODULE :=
include $(ESXDP_ROOT)/config.mk
override MODULE := $(_MODULE)

OUT_ROOT = build/$(DEV_BUILDTYPE)/$(ESX_TARGET)

# commands
#CC = gcc
#CXX = g++
#AR = ar	
#AS = as
#VMKLD = $(BINUTILS_PREFIX)ld

# (VMW_CC_UW) -c -o $@ $(VMW_REL_FLAGS) $(VMW_UFLAGS) $(DEV_$(DEV_BUILDTYPE)_COMPILEFLAGS) $(VMW_INC) $(abspath $<)

CC = $(VMW_vmkernel64_$(DEV_BUILDTYPE)_CCBIN)
CXX = $(VMW_vmkernel64_$(DEV_BUILDTYPE)_CCBIN)

ifeq ($(CAYMAN_ESX_TOOLCHAIN_DIR),)
AR = $(BINUTILS)/bin/x86_64-linux-ar
else
AR = $(CAYMAN_ESX_TOOLCHAIN_DIR)/usr/bin/x86_64-vmk-linux-gnu-ar
endif
VMKLD := /build/toolchain/lin64/binutils-2.25/x86_64-linux5.0/bin/ld
LD := $(CC)

CFLAGS_ALL = $(subst -MD -MP -MT $@ -MF $@.d,,$(DEV_$(DEV_BUILDTYPE)_COMPILEFLAGS))

CFLAGS_UW =  $(VMW_REL_FLAGS) $(VMW_INC) $(VMW_UFLAGS)\
	$(VMW_uw$(DEV_TARGET_BITNESS)_$(DEV_BUILDTYPE)_COMPILEFLAGS) \
	$(DEV_uw_$(DEV_BUILDTYPE)_COMPILEFLAGS)

CFLAGS_VMKMOD = $(VMW_vmkernel64_$(DEV_BUILDTYPE)_COMPILEFLAGS) \
	$(DEV_vmkernel64_$(DEV_BUILDTYPE)_COMPILEFLAGS) \
	$(VMW_REL_FLAGS) $(VMW_KFLAGS) $(VMW_INC) 

CFLAGS = $(CFLAGS_ALL) $(CFLAGS_UW)

CP = cp
MKDIR = mkdir -p
RM = rm -f 
RMDIR = rm -fr
INSTALL = install

CCOMPILE   = $(CC) -c -MMD -MP -MT $@ -MF $@.d $(CFLAGS) $(CPPFLAGS) -o $@ $<
CXXCOMPILE = $(CXX) -c -MMD -MP -MT $@ -MF $@.d $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<
MKLIB      = rm -fr $@ && $(AR) cr $@ $(OBJS)
MKDLL      = $(LD)  -o $@ -shared -Wl,-soname=$(call xsoname, $@) \
	-Wl,--whole-archive $(OBJS) -Wl,--no-whole-archive \
	-Wl,-rpath=$(OUT_ROOT)/lib $(DEPLIBS) $(DEPDLLLIBS) $(LDFLAGS)
MKVMKMOD   = $(VMKLD) -Ur -o $@ --whole-archive  $(OBJS) --no-whole-archive $(VMKDEPLIBS) \
	$(LDFLAGS)
MKPROG     = $(LD) -o $@ $(OBJS) $(DEPLIBS) $(DEPDLLLIBS) -Wl,-rpath=$(OUT_ROOT)/lib $(LDFLAGS) 

MKINSTALL    = ln -sf $(abspath $<) $@

#names
O_EXT = .o
LIB_EXT = .a
PROG_EXT = 
DLL_EXT = .so

xlibname  = $(OUT_ROOT)/lib/$(1)$(LIB_EXT)
xprogname = $(OUT_ROOT)/bin/$(1)$(PROG_EXT)
xdllname  = $(OUT_ROOT)/lib/lib$(1)$(DLL_EXT)
xdlllib   = $(OUT_ROOT)/lib/lib$(1)$(DLL_EXT)
xsoname   = $(1:$(OUT_ROOT)/lib/%=%)
xvmkmodname   = $(OUT_ROOT)/vib/stage/usr/lib/vmware/vmkmod/nsx-$(1)


#flags
CPPFLAGS += -I$(OUT_ROOT)/include

COPTFLAGS = -O3

CXXFLAGS += -std=gnu++0x

LDFLAGS += -L$(OUT_ROOT)/lib
LDFLAGS += -g

XO_SUFFIXES += vmklibs vmkmods

####################################################################################
# $1 - module
# $2 - lib
define make-vmklib
$(trace)
$(call make-bin,$1,$2,$(call xlibname,$(2)),$(value MKLIB),vmklibs)
$(call make-hdrs,$(1),$(2))
$1_VMKEXPLIBS += $(call xlibname,$(2))
$$($1_$2_OBJS): CFLAGS := $(CFLAGS_ALL) $(CFLAGS_VMKMOD)
endef

####################################################################################
# $1 - module
# $2 - dll
define make-vmkmod
$(trace)
$(call make-bin,$1,$2,$(call xvmkmodname,$(2)),$(value MKVMKMOD),vmkmods)
$(call make-hdrs,$(1),$(2))
$$($1_$2_OBJS): CFLAGS := $(CFLAGS_ALL) $(CFLAGS_VMKMOD)
endef

define make-vmktest
$(trace)
$(call make-bin-int,$(1),vmktest,$(call xvmkmodname,$(1)-vmktest),$(value MKVMKMOD),vmktests,$($(1)_VMKTESTS))
$(call xvmkmodname,$(1)-vmktest): VMKDEPLIBS = $$($1_VMKDEPLIBS) $$($1_$2_DEPLIBS) $$($1_VMKEXPLIBS)
$(call xvmkmodname,$(1)-vmktest): $$($(1)_VMKDEPLIBS) $$($1_$2_DEPLIBS) $$($1_VMKEXPLIBS)
$(1)-tests: $(call xvmkmodname,$(1)-vmktest)
$$($1_vmktest_OBJS): CFLAGS := $(CFLAGS_ALL) $(CFLAGS_VMKMOD)
endef

####################################################################################
define esx-hook
$(trace)
.PHONY: $(MODULE)-vmkmods $(MODULE)-vmklibs
$(MODULE)-build: $(MODULE)-vmkmods $(MODULE)-vmklibs
$(foreach vmklib, $($(MODULE)_VMKLIBS),$(call make-vmklib,$(MODULE),$(vmklib)))
$(foreach vmkmod, $($(MODULE)_VMKMODS),$(call make-vmkmod,$(MODULE),$(vmkmod)))
$(if $($(MODULE)_VMKTESTS),
$(eval $1_vmktest_SRCS := $($1_VMKTESTS))
$(call make-vmktest,$(MODULE)))
endef

define esx-import-hook
$(MODULE)_VMKDEPLIBS += $($(1)_VMKEXPLIBS)
$(MODULE)_VMKEXPLIBS := $($(1)_VMKDEPLIBS)
endef


XO_HOOKS += esx-hook
XO_IMPORT_HOOKS += esx-import-hook
