# commands
CC = cl
CXX = cl
AR = lib
LD = cl
AS = ml

SRC_ROOT := $(shell fixpath $(SRC_ROOT))

ifeq ($(OUT_ROOT),)
OUT_ROOT=$(SRC_ROOT)/out
endif
ifeq ($(TMP_ROOT),)
TMP_ROOT=$(OUT_ROOT)/tmp
endif

TMP_ROOT := $(shell fixpath $(TMP_ROOT))
OUT_ROOT := $(shell fixpath $(OUT_ROOT))

CP = $(XO_ROOT)/win32/cp
MKDIR = $(XO_ROOT)/win32/mkdir -p 
RM = $(XO_ROOT)/win32/rm
RMDIR = $(XO_ROOT)/win32/rm -fr
TAR = $(XO_ROOT)/win32/tar 
GZIP = $(XO_ROOT)/win32/gzip 
GUNZIP = $(XO_ROOT)/win32/gunzip
ZIP = $(XO_ROOT)/win32/zip 
UNZIP = $(XO_ROOT)/win32/unzip
BZIP = $(XO_ROOT)/win32/bzip2
BUNZIP = $(XO_ROOT)/win32/bunzip2

export C_INCLUDE_PATH = $(INCLUDE)
MAKEDEPEND = $(XO_ROOT)/win32/makedepend $(MAKEDEPENDFLAGS)
MAKEDEF = $(XO_ROOT)/win32/makedef

MKLIB      = $(AR) /nologo /OUT:$@ $(OBJS)
MKPROG     = $(LD) $(OBJS) $(DEPLIBS) $(DEPDLLLIBS) /link $(EXTRALIBS) $(LDFLAGS) /OUT:$@ 
INSTALL    = $(XO_ROOT)/win32/install $< $@

define CCOMPILE 
	$(MAKEDEPEND) $(CPPFLAGS) $(MAKEDEPENDFLAGS)  $<
	$(CC) -c $(CFLAGS) $(CPPFLAGS) -Fo$@ $(SRC_ABSROOT)/$<
endef

define CXXCOMPILE 
	$(MAKEDEPEND) $(CPPFLAGS) $(MAKEDEPENDFLAGS) $<
	$(CXX) -c -TP $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -Fo$@ $(SRC_ABSROOT)/$<
endef

define MKDLL
	$(MAKEDEF) /O:$(INT_DIR)/$(TARGET).def $(OBJS) $(DEPLIBS)
	$(LD) $(OBJS) $(DEPLIBS) $(DEPDLLLIBS) /link /DLL /DEF:$(INT_DIR)/$(TARGET).def /OUT:$@ \
		/IMPLIB:$(OUT_ROOT)/lib/$(TARGET).lib $(EXTRALIBS) $(LDFLAGS)
endef


O_EXT = .obj
LIB_EXT = .lib
PROG_EXT = .exe
DLL_EXT = .dll

xlibname = $(OUT_ROOT)/lib/$(1)$(LIB_EXT)
xprogname = $(OUT_ROOT)/bin/$(1)$(PROG_EXT)
xdllname = $(OUT_ROOT)/bin/$(1)$(DLL_EXT)
xdlllib = $(OUT_ROOT)/lib/$(1)$(LIB_EXT)


# flags
MAKEDEPENDFLAGS = -D_MSC_VER=1100 -D_M_IX86 -D__cplusplus -o$(suffix $<).obj -f$@.d -p$(INT_ROOT)/

CPPFLAGS += -I$(INT_DIR) -I$(OUT_ROOT)/include
CPPFLAGS += -DWIN32 -D_WIN32 -D_MBCS
CFLAGS = /nologo /W3 
#/WX 
LDFLAGS = /nologo /LIBPATH:$(OUT_ROOT)/lib
EXTRALIBS = kernel32.lib user32.lib gdi32.lib \
	winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib \
	iphlpapi.lib wininet.lib crypt32.lib version.lib rpcrt4.lib ws2_32.lib setupapi.lib

ifdef DEBUG
CPPFLAGS += -D_DEBUG 
CFLAGS += /MDd /Zi /GZ 
LDFLAGS += /DEBUG
else
CPPFLAGS += -DNDEBUG 
CFLAGS += /MD /O2 
endif


$(INT_ROOT)/%.rc$(O_EXT): $(SRC_ROOT)/%.rc | $$(@D)/.
	$(MAKEDEPEND) $(CPPFLAGS) $(MAKEDEPENDFLAGS) $<
	rc $(CPPFLAGS) -Fo$@ $(SRC_ABSROOT)/$<


# other

define make-dll-lib-rule
$(call xdlllib,$2): $(call xdllname,$2)
endef


#idl support
define make-idl-output
$(INT_DIR)/$(basename $1).h $(INT_DIR)/$(basename $1)_i.c $(INT_DIR)/$(basename $1).tlb
endef

# midl rule
$(INT_ROOT)/%_i.c $(INT_ROOT)/%.h $(INT_ROOT)/%.tlb: $(SRC_ROOT)/%.idl  | $$(@D)/.
	midl /nologo /tlb $(INT_ROOT)/$*.tlb /h $(INT_ROOT)/$*.h /iid $(INT_ROOT)/$*_i.c /Oicf $<

####################################################################################
define make-idl
$(xtrace)

.PHONY: $(1)-$(2)-idl

$(1)-idl: $(1)-$(2)-idl 

$(1)-$(2)-idl: $(call make-idl-output,$2) $(OUT_ROOT)/bin/$(basename $2).tlb

$(OUT_ROOT)/bin/$(basename $2).tlb: $(INT_DIR)/$(basename $2).tlb
	-$(RM) $$@ 
	$$(INSTALL)

endef

XO_HOOKS += xo-idl
define xo-idl
$(xtrace)

.PHONY: $(MODULE)-idl

$(MODULE)-idl:

$(MODULE)-deps: $(MODULE)-idl

$(foreach idl,$($(MODULE)_IDLS),$(call make-idl,$(MODULE),$(idl)))
endef
